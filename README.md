# SkyRoof Project

###Security howto use
####{baseUrl}/authenticate
Use POST request and pass this as body with no authentication
```
{
    "username": "username here",
    "password": "password here"
}
```

###Register new user
####{baseUrl}/register
Use POST request and pass this as body with no authentication
```
{
    "username": "new user's username",
    "password": "new password",
    "email": "newuser@example.com"
}
```

####Use other requests
Go to Postman `Authorization` tab. Go to `TYPE` field and select `Bearer Token` then paste in `Token` field the value returned by `/authentication` POST request 

### Paths 

###Issues
* #####{baseUrl}/api/issue/get/all : ```GET``` 
Returns all the Issues.
* #####{baseUrl}/api/issue/get/id/{id} : ```GET``` 
Return specific ```Issue``` given an ```{id}```
* #####{baseUrl}/api/issue/save : ```POST``` 
Requires a body of ```Issue```  without ```issueID```.
```
{
    "project": {
        "projectID": 1
    },
    "title": " A must have title ",
    "description": "if you want describe",
    "assignor": {
        "userID": 1
    	
    },
    "assignee": {
        "userID": 1
    },
    "type": "OTHER",
    "otherDetails": "If you want",
    "status": "REOPEN"
}
```
* #####{baseUrl}/api/issue/update : ```PUT``` 
Requires a body of ```Issue```  with ```issueID```.
```
{
    "issueId": 1,
    "title": "New example title"
}
```
* #####{baseUrl}/api/issue/delete : ```DELETE```
Requires a body of ```Issue```  with ```issueID```. Updates ```status``` field to ```DELETED```.
```
{
    "issueId": 1
}
```


###Project
* #####{baseUrl}/api/project/get/all : ```GET``` 
Returns all the projects.
* #####{baseUrl}/api/project/get/id/{id} : ```GET``` 
Return specific ```Project``` given an ```{id}```

* #####{baseUrl}/api/project/save : ```POST``` 
Requires a body of ```Project```  without ```projectID```.
```
{
    "name":"New Project example"
}
```
* #####{baseUrl}/api/project/update : ```PUT``` 
Requires a body of ```Project```  with ```projectID```.
```
{
    "projectID": 1, //Required
    "name":"New Project example"
}
```


###Users
* #####{baseUrl}/api/user/get/all : ```GET``` 
Returns all the Issues.
* #####{baseUrl}/api/user/get/id/{id} : ```GET``` 
Return specific ```User``` given an ```{id}```

* #####{baseUrl}/api/user/save : ```POST``` 
Requires a body of ```User```  without ```userID```.
```
{
    "username": "exampleuser",
    "password" : 12345,
    "permission": "READ",
    "email": "email@email.com"
}
```
* #####{baseUrl}/api/user/update : ```PUT``` 
Requires a body of ```User```  with ```userID```.
```
{
    "userID": 65, //REQUIRED 
    "username": "newUsername",
}
```

 
###Autocomplete
* #####/api/autocomplete/assignor/other : ```POST``` 
Requires a body of ```User``` returns a ```List(User)``` which contains a ```User: assignor``` in index:0 and ```User: assignee```in index:1.
```$xslt
  
```
* #####/api/autocomplete/assignee : ```POST``` 
Requires a body of ```User```, returns a ```List(User)``` which contains all assignees (```User```) .


* #####/api/autocomplete/project : ```POST```
Requires a body of ```String```, returns a ```List(Project)```.

* #####/api/autocomplete/assignor : ```POST``` 
Requires a body of ```User```, returns a ```List(User)``` which contains all assignors (```User```) .


* #####/api/autocomplete/type : ```GET``` 
Returns a ```List(String)``` which contains all possible ```TYPE```.

###Search 
* #####/api/search : ```POST``` 
Can requires a body of:
* ```title(String)``` being mandatory.3
* AND/OR ```assignor(User)``` with fields ```username``` being mandatory.
* AND/OR ```assignee(User)``` with fields ```username``` being mandatory.
* AND/OR requires a body of ```project(Project)``` with fields ```name``` being mandatory. 
* AND/OR requires a body of ```type``` (```Enum```) being mandadory
* AND/OR requires a body of ```status``` (```Enum```) being mandadory
```
{
    "assignor": {
        "username": "example1"
    },
    "assignee": {
        "username": "example2"
    },
    "project": {
        "name": "example3"
    },
    "status": 3, // 0-3 or Enum fields
    "type": 2, // 0-2 or Enum fields
    "title": "example_title"
}
```
- Returns ```List(Issues)```
* #####/api/homepage : ```POST``` 
Requires a body of ```Users``` with ```userID``` being mandatory. 
```
{
 	"userID": 11
}
```
- Returns a ```List(Issue)``` where the issues are not deleted nad assignee or assignor matches the ```User```.


