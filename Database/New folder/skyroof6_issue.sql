-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: skyroof6
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `issue`
--

DROP TABLE IF EXISTS `issue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `issue` (
  `issueID` bigint(20) NOT NULL,
  `projectID` bigint(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `assignor` bigint(20) NOT NULL,
  `assignee` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  `other_details` text,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`issueID`),
  KEY `assignor` (`assignor`),
  KEY `assignee` (`assignee`),
  KEY `FKj5jkwvvf243vkw3ma2k0f4f9p` (`projectID`),
  CONSTRAINT `FKj5jkwvvf243vkw3ma2k0f4f9p` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`),
  CONSTRAINT `issue_ibfk_1` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `issue_ibfk_2` FOREIGN KEY (`assignor`) REFERENCES `user` (`userID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `issue_ibfk_3` FOREIGN KEY (`assignee`) REFERENCES `user` (`userID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `chk_status` CHECK ((`status` in (0,1,2,3,4))),
  CONSTRAINT `CHK_TYPE` CHECK ((`type` in (0,1,2)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issue`
--

LOCK TABLES `issue` WRITE;
/*!40000 ALTER TABLE `issue` DISABLE KEYS */;
INSERT INTO `issue` VALUES (1,1,'100kg Bricks','Stronger Bricks',11,11,2,'Up to 30/12/2019',0),(2,2,'Renovate Entrance','Bigger doors and windows',13,17,1,'Nothing',1),(3,3,'1 Tone Metal','n/a',14,18,0,'Nothing',0),(4,4,'Lightning Rod','Immediate Installation',15,19,1,'Nothing',2),(5,5,'500kg Gravel','n/a',16,21,2,'Good Quality',1),(6,6,'Recruiting Employees','Mostly Junior',13,11,0,'30 People ',1);
/*!40000 ALTER TABLE `issue` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 22:20:24
