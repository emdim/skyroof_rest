-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: skyroof6
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `userID` bigint(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `permission` int(11) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username` (`username`),
  CONSTRAINT `CHK_PERMISSION` CHECK ((`PERMISSION` in (0,1,2)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'harrysluyr','$2a$10$N0SkJL1TRK3rdlCCzgNHpuChbxqG6he7Gzt.oWJG5Ff.mmRF1JGMy','email@email.com',0),(9,'','12345','email@email.com',0),(10,'uruwet','','email@email.com',0),(11,'KostasBen','$2a$10$CJ.hvgYgEmzNhN6rfAnEReAI8w2FZqtWMIwuEzD8K6qv4.DifNfmy','kostas@gmail.com',1),(13,'rtsolka','$2a$10$miQZDzBQgcv9TmgRBcX8zOyI/K4gdKpJR9F9wlP3GNPTM2YH1by1e','rtsolka@skyroof.com',2),(14,'ManolisDim','$2a$10$uSi6jpt6iZH2/yD3En2s2ObSUXhEXNIMwnwcSpC9xUykzsdw03tLy','manos_dim@skyroof.com',2),(15,'xaris.Lor','$2a$10$2K9iC/.HO6fZP7i/Y.SwjejUC1AstEJjL/lpZkmCHRk7tmzKW2zdS','xaris@hotmail.com',1),(16,'Bill','$2a$10$Coql4PDtY6kIknVdOFLe2euqFmY.9gc163a04ctrrWmVEVyb9JPK2','skiadas@skyroof.com',2),(17,'ben10','$2a$10$ouroiu9qLels8XpbXuKoHO5QhXv8W93oHh2wen0D3kt2sk5D7zsuG','ben10@gmail.gr',0),(18,'ourania','$2a$10$P3pmjb2JFOQX9l4EOHqFku7/iK8Fu/EJKnJxfz7tqEAtGGc4UrmuS','ourania@intrasoft.gr',0),(19,'emanouil','$2a$10$BZkAyVGWdQSjJXeY8kAzrOPRLbhWWquNNnfrAhOukyvtryej5TcrK','manos@yahoo.gr',0),(21,'xaralampos','$2a$10$8p6T9GSMWRaNWZOt4mb1ku4RcDC0NArdrwUPNxtviqAzZz1vTkNOW','xaris11@hotmail.com',0),(22,'vasilis','$2a$10$t85Ih9ku/fmQ1O338jIYRu9KWhTJI0oMGgt/JPWf68wO4Sx7ufxqS','vasilis@gmail.com',0),(25,'jiljljlkkkklll;l;;l','sdsad','email@email.com',0),(33,'ooo','dfhjk','email2@email.com',0),(37,'uhuk','$2a$10$Fk1tkbOfnegzZQMMWvLCfOwKqyhTKsvHuKFhEi6FjAmNueGbYcqS6','email@email.com',0),(42,'hihi','$2a$10$PUjpy0MmImPKBWNkH5EOt.k2ZMv/.Ns.LDq1Q9Vbemhn/ob8yYLoy','email3@email.com',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 22:20:25
