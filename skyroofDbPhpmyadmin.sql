-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2019 at 06:01 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skyroof2`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(33),
(33),
(33);

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

CREATE TABLE `issue` (
  `issueID` bigint(20) NOT NULL,
  `projectID` bigint(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `assignor` bigint(20) NOT NULL,
  `assignee` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `other_details` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `issue`
--

INSERT INTO `issue` (`issueID`, `projectID`, `title`, `description`, `assignor`, `assignee`, `type`, `status`, `other_details`) VALUES
(1, 1, '100kg Bricks', '2', 11, 11, 2, 2, 'Up to 30/12/2019'),
(2, 2, 'Renovate Entrance', '1', 13, 17, 1, 1, 'Nothing'),
(3, 3, '1 Tone Metal', 'tyuuyuytuty', 14, 17, 1, 1, 'Nothing'),
(4, 4, 'Lightning Rod', '2', 15, 19, 1, 2, 'Nothing'),
(5, 5, '500kg Gravel', '1', 16, 21, 2, 1, 'Good Quality'),
(6, 6, 'Recruiting Employees', '1', 13, 11, 0, 1, '30 People '),
(24, 1, 'NOT ENOUGH BRICKS', '0', 14, 11, 1, 0, 'BRICKS ALSO NEED TO BE OF HIGH QUALITY'),
(27, 1, 'qweqweqw', '0', 14, 13, 1, 0, 'qweqwe'),
(28, 2, 'test issue', '0', 14, 11, 0, 0, 'eddsasda'),
(29, 3, 'dsaasd', '0', 14, 17, 1, 0, 'sadas'),
(30, 3, 'dsadas', '0', 14, 17, 1, 0, 'dsadas'),
(31, 5, 'gfgfdgfd', '0', 14, 18, 2, 0, 'fdggfdfgd'),
(32, 3, 'ussue name here', 'decription here', 14, 15, 2, 0, 'additional info');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `name` varchar(50) DEFAULT NULL,
  `projectID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`name`, `projectID`) VALUES
('Τown Ηall in Greece', 1),
('Miami Skyscraper', 2),
('Arab Emirate Palace', 3),
('London Bridge', 4),
('Eiffel Tower', 5),
('Intrasoft Patras', 6);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID` bigint(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `permission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `username`, `password`, `email`, `permission`) VALUES
(11, 'KostasBen', '$2a$10$CJ.hvgYgEmzNhN6rfAnEReAI8w2FZqtWMIwuEzD8K6qv4.DifNfmy', 'kostas@gmail.com', 1),
(13, 'rtsolka', '$2a$10$miQZDzBQgcv9TmgRBcX8zOyI/K4gdKpJR9F9wlP3GNPTM2YH1by1e', 'rtsolka@skyroof.com', 2),
(14, 'ManolisDim', '$2a$10$uSi6jpt6iZH2/yD3En2s2ObSUXhEXNIMwnwcSpC9xUykzsdw03tLy', 'manos_dim@skyroof.com', 2),
(15, 'xaris.Lor', '$2a$10$2K9iC/.HO6fZP7i/Y.SwjejUC1AstEJjL/lpZkmCHRk7tmzKW2zdS', 'xaris@hotmail.com', 1),
(16, 'Bill', '$2a$10$Coql4PDtY6kIknVdOFLe2euqFmY.9gc163a04ctrrWmVEVyb9JPK2', 'skiadas@skyroof.com', 2),
(17, 'ben10', '$2a$10$ouroiu9qLels8XpbXuKoHO5QhXv8W93oHh2wen0D3kt2sk5D7zsuG', 'ben10@gmail.gr', 0),
(18, 'ourania', '$2a$10$P3pmjb2JFOQX9l4EOHqFku7/iK8Fu/EJKnJxfz7tqEAtGGc4UrmuS', 'ourania@intrasoft.gr', 0),
(19, 'emanouil', '$2a$10$BZkAyVGWdQSjJXeY8kAzrOPRLbhWWquNNnfrAhOukyvtryej5TcrK', 'manos@yahoo.gr', 0),
(21, 'xaralampos', '$2a$10$8p6T9GSMWRaNWZOt4mb1ku4RcDC0NArdrwUPNxtviqAzZz1vTkNOW', 'xaris11@hotmail.com', 0),
(22, 'vasilis', '$2a$10$t85Ih9ku/fmQ1O338jIYRu9KWhTJI0oMGgt/JPWf68wO4Sx7ufxqS', 'vasilis@gmail.com', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `issue`
--
ALTER TABLE `issue`
  ADD PRIMARY KEY (`issueID`),
  ADD KEY `assignor` (`assignor`),
  ADD KEY `assignee` (`assignee`),
  ADD KEY `FKj5jkwvvf243vkw3ma2k0f4f9p` (`projectID`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`projectID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `issue`
--
ALTER TABLE `issue`
  ADD CONSTRAINT `FKj5jkwvvf243vkw3ma2k0f4f9p` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`),
  ADD CONSTRAINT `issue_ibfk_1` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`),
  ADD CONSTRAINT `issue_ibfk_2` FOREIGN KEY (`assignor`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `issue_ibfk_3` FOREIGN KEY (`assignee`) REFERENCES `user` (`userID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
