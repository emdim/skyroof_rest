import React, {Component} from "react";
import Search from "../../components/Search/Search";
import MenuItem from "@material-ui/core/MenuItem";
import * as actions from "../../actions";
import {connect} from "react-redux";

class SearchHOC extends Component{
    constructor(props){
        super(props);
        this.state = {
            project: '',
            issue: '',
            assignor: this.props.user.username, //must have connected userID by default
            assignee: '',
            type: '',
            status: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleClear = this.handleClear.bind(this);
        this.createMenuItem = this.createMenuItem.bind(this);
        this.createMenuItemWithId = this.createMenuItemWithId.bind(this);
        this.handleSearchMyIssues = this.handleSearchMyIssues.bind(this);
        this.handleSearchOpenIssues = this.handleSearchOpenIssues.bind(this);
    }

    componentDidMount(){
        this.props.initSearchPage();
        this.props.fetchAllUsers();
        this.props.fetchProjects();
    }

    createMenuItem = function (items, idColumn, titleColumn){
        return items.map((item) => (
            <MenuItem key={item[idColumn]} value={item[titleColumn]}>{item[titleColumn]}</MenuItem>
        ));
    };

    createMenuItemWithId = function (items, idColumn, titleColumn){
        return items.map((item) => (
            <MenuItem key={item[idColumn]} value={item[idColumn]}>{item[titleColumn]}</MenuItem>
        ));
    };

    handleInputChange = function (event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    };

    handleClear = function (event) {
        event.preventDefault();

        this.setState({
            project: '',
            issue: '',
            assignor: this.props.user.username,
            assignee: '',
            type: '',
            status: ''
        });
    };

    handleSubmit = function (event) {
        event.preventDefault();
        let status = null;
        if(this.state.status){
            status = this.state.status
        }
        let type = null;
        if(this.state.type){
            type = this.state.type
        }
        this.props.onSubmitSearch({
            sendBody: {
                "assignor": {
                    "username": this.state.assignor
                },
                "assignee": {
                    "username": this.state.assignee
                },
                "project": {
                    "name": this.state.project,
                },
                "status": status,
                "type": type,
                "title": this.state.issue
            }
        });
    };

    handleSearchMyIssues = function(event){
        event.preventDefault();

        this.props.searchMyIssues({
            sendBody: {
                "userID": this.props.user.userId
            }
        });
    };

    handleSearchOpenIssues = function(event){
        event.preventDefault();

        this.props.onSubmitSearch({
            sendBody: {
                "assignor": {
                    "username": ""
                },
                "assignee": {
                    "username": ""
                },
                "project": {
                    "name": "",
                },
                "status": 0,
                "type": null,
                "title": ""
            }
        });
    };

    render() {
        const allUsers = this.props.allUsers;
        const projects = this.props.projects;
        return(
            <Search
                {...this.props}

                handleSubmit={this.handleSubmit}
                handleClear={this.handleClear}
                handleInputChange={this.handleInputChange}
                createMenuItem = {this.createMenuItem}
                createMenuItemWithId = {this.createMenuItemWithId}
                handleSearchMyIssues = {this.handleSearchMyIssues}
                handleSearchOpenIssues = {this.handleSearchOpenIssues}

                project={this.state.project}
                issue={this.state.issue}
                assignor={this.state.assignor}
                assignee={this.state.assignee}
                type={this.state.type}
                status={this.state.status}

                projectList={projects}
                assignorList={allUsers}
                assigneeList={allUsers}

                statusList={[
                    {id:'0',title:'Open'},
                    {id:'1',title:'Reopened'},
                    {id:'2',title:'Resolved'},
                    {id:'3',title:'Closed'},
                ]}

                typeList={[
                    {id:'ERROR',title:'Error'},
                    {id:'IMPROVEMENT',title:'Improvement'},
                    {id:'OTHER',title:'Other'},
                ]}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        // message: state.issues.message,
        // error: state.issues.error,
        user: state.user,
        allUsers: state.allUsers.byIds,
        projects: state.projects.byIds,
        searchResultIssues: state.issues.searchIssues,
        allIssues: state.issues.byId,
        searchLoading: state.issues.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        initSearchPage: (payload) => dispatch(actions.initSearchPage(payload)),
        fetchAllUsers: (payload) => dispatch(actions.fetchAllUsers(payload)),
        fetchProjects: (payload) => dispatch(actions.fetchProjects(payload)),
        onSubmitSearch: (payload) => dispatch(actions.searchIssues(payload)),
        searchMyIssues: (payload) => dispatch(actions.searchMyIssues(payload))
    }
};
export default connect(mapStateToProps,mapDispatchToProps)(SearchHOC);