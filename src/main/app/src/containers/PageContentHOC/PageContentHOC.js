import React, {Component} from "react";
import PageContent from "../../components/PageContent/PageContent";

class PageContentHOC extends Component{
    render() {
        return(
            <PageContent>
                {this.props.children}
            </PageContent>
        )
    }
}

export default PageContentHOC;