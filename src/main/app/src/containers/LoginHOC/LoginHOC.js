import React, {Component} from "react";
import Login from "../../components/Login/Login";
import * as actions from '../../actions';
import {connect} from "react-redux";

class LoginHOC extends Component{
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: ''
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClear = this.handleClear.bind(this);
    }

    handleInputChange = function (event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    };

    handleSubmit = function (event) {
        event.preventDefault();

        this.props.onLogin({
            sendBody: {
                username: this.state.username,
                password: this.state.password
            }
        });

    };

    handleClear = function (event) {
        event.preventDefault();
        this.setState({
            username: '',
            password: ''
        });
    };

    render() {
        return(
            <Login
                {...this.props}
                username={this.state.username}
                password={this.state.password}
                handleSubmit={this.handleSubmit}
                handleInputChange={this.handleInputChange}
                handleClear={this.handleClear}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.user.loading,
        error: state.user.error,
        isAuthenticated: state.user.token !== null
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (payload) => dispatch(actions.loginStart(payload))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(LoginHOC);