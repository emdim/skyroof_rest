import React, {Component} from "react";
import Homepage from "../../components/Homepage/Homepage";
import * as actions from "../../actions";
import {connect} from "react-redux";

class HomepageHOC extends Component {
    componentDidMount() {
        setTimeout(() => {
            this.props.searchMyIssues({
                sendBody: {
                    "userID": this.props.user.userId
                }
            });
        },0);

    }

    render() {
        const {issues, user} = this.props;

        return <Homepage
                    myIssues={issues}
                    user={user}
                    location={this.props.location}
                    searchMyIssues={this.props.searchMyIssues}
               />
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
        issues: state.issues.myIssues
    }
};

const mapDispatchToProps = dispatch => {
    return {
        searchMyIssues: (payload) => dispatch(actions.searchMyIssues(payload)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomepageHOC);