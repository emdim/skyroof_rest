import React, {Component} from "react";
import CreateUpdateForm from "../../components/CreateUpdate/CreateUpdateForm";
import MenuItem from "@material-ui/core/MenuItem";
import * as actions from "../../actions";
import {connect} from "react-redux";

class CreateHOC extends Component {
    constructor(props){
        super(props);
        this.state = {
            project: '',
            issue: '',
            description: '',
            assignor: this.props.userId, //must have connected userID by default
            assignee: '',
            status: 'OPEN', //must have OPEN id as default value
            category: '',
            additionalInfo: '',
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleClear = this.handleClear.bind(this);
        this.createMenuItem = this.createMenuItem.bind(this);
    }

    componentDidMount(){
        this.props.fetchAllUsers();
        this.props.fetchProjects();
    }

    createMenuItem = function (items, idColumn, titleColumn){
        return items.map((item) => (
            <MenuItem key={item[idColumn]} value={item[idColumn]}>{item[titleColumn]}</MenuItem>
        ));
    };

    handleSubmit = function (event) {
        event.preventDefault();

        this.props.onSubmit({
            sendBody:{
                project: {
                    projectID: this.state.project
                },
                title: this.state.issue,
                description: this.state.description,
                assignor: {
                    userID: this.state.assignor
                },
                assignee: {
                    userID: this.state.assignee
                },
                type: this.state.category,
                status: this.state.status,
                otherDetails: this.state.additionalInfo
            }
        });

    };

    handleClear = function (event) {
        event.preventDefault();

        this.setState({
            project: '',
            issue: '',
            description: '',
            assignor: this.props.userId, //set here the current connected user
            assignee: '',
            status: 'OPEN', //set here the open status
            category: '',
            additionalInfo: '',
        });
    };

    handleInputChange = function (event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    };

    render() {
        const allUsers = this.props.allUsers;
        const projects = this.props.projects;

        return(
            <CreateUpdateForm
                {...this.props}

                handleSubmit={this.handleSubmit}
                handleClear={this.handleClear}
                handleInputChange={this.handleInputChange}
                createMenuItem = {this.createMenuItem}

                project={this.state.project}
                issue={this.state.issue}
                description={this.state.description}
                assignor={this.state.assignor}
                assignee={this.state.assignee}
                status={this.state.status}
                category={this.state.category}
                additionalInfo={this.state.additionalInfo}
                pageTitle="Create Issue"
                isUpdatePage={false}

                projectList={projects}
                assignorList={allUsers}
                assigneeList={allUsers}

                statusList={[
                    {id:'OPEN',title:'Open'},
                    {id:'CLOSED',title:'Closed'},
                    {id:'REOPEN',title:'Reopened'},
                    {id:'RESOLVED',title:'Resolved'},
                ]}

                categoryList={[
                    {id:'ERROR',title:'Error'},
                    {id:'IMPROVEMENT',title:'Improvement'},
                    {id:'OTHER',title:'Other'},
                ]}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        message: state.issues.message,
        error: state.issues.error,
        userId: state.user.userId,
        allUsers: state.allUsers.byIds,
        projects: state.projects.byIds
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchAllUsers: (payload) => dispatch(actions.fetchAllUsers(payload)),
        fetchProjects: (payload) => dispatch(actions.fetchProjects(payload)),
        onSubmit: (payload) => dispatch(actions.createIssue(payload)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateHOC);