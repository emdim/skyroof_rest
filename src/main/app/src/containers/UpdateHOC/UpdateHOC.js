import React, {Component} from "react";
import CreateUpdateForm from "../../components/CreateUpdate/CreateUpdateForm";
import MenuItem from "@material-ui/core/MenuItem";
import * as actions from "../../actions";
import {connect} from "react-redux";
import {Redirect} from "react-router";

class UpdateHOC extends Component {
    constructor(props){
        super(props);

        if(this.props.issues) {
            const {issueToUpdateId} = this.props.match.params;
            const issueToUpdate = this.props.issues[issueToUpdateId];


            this.state = {
                issueId: issueToUpdate.id,
                project: issueToUpdate.projectId,
                issue: issueToUpdate.title,
                description: issueToUpdate.description,
                assignor: issueToUpdate.assignorId, //must have connected userID by default
                assignee: issueToUpdate.assigneeId,
                status: issueToUpdate.status, //must have OPEN id as default value
                category: issueToUpdate.type,
                additionalInfo: issueToUpdate.otherDetails,
                cancel: false
            };
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.createMenuItem = this.createMenuItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentDidMount(){
        this.props.fetchAllUsers();
        this.props.fetchProjects();
    }

    createMenuItem = function (items, idColumn, titleColumn){
        return items.map((item) => (
            <MenuItem key={item[idColumn]} value={item[idColumn]}>{item[titleColumn]}</MenuItem>
        ));
    };

    handleSubmit = function (event) {
        event.preventDefault();

        this.props.onSubmit({
            sendBody:{
                issueId: this.state.issueId,
                project: {
                    projectID: this.state.project
                },
                title: this.state.issue,
                description: this.state.description,
                assignor: {
                    userID: this.state.assignor
                },
                assignee: {
                    userID: this.state.assignee
                },
                type: this.state.category,
                status: this.state.status,
                otherDetails: this.state.additionalInfo
            }
        });

    };

    handleInputChange = function (event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    };

    handleCancel = function (event) {
        this.setState({cancel: true})
    };

    render() {
        const allUsers = this.props.allUsers;
        const projects = this.props.projects;

        if(!this.props.issues){
            return (<Redirect to="/"/>);
        }

        return(
            <CreateUpdateForm
                {...this.props}

                handleSubmit={this.handleSubmit}
                handleInputChange={this.handleInputChange}
                handleCancel={this.handleCancel}
                createMenuItem = {this.createMenuItem}

                project={this.state.project}
                issue={this.state.issue}
                description={this.state.description}
                assignor={this.state.assignor}
                assignee={this.state.assignee}
                status={this.state.status}
                category={this.state.category}
                additionalInfo={this.state.additionalInfo}
                pageTitle="Update Issue"
                isUpdatePage={true}
                cancel={this.state.cancel}

                projectList={projects}
                assignorList={allUsers}
                assigneeList={allUsers}

                statusList={[
                    {id:'OPEN',title:'Open'},
                    {id:'CLOSED',title:'Closed'},
                    {id:'REOPEN',title:'Reopened'},
                    {id:'RESOLVED',title:'Resolved'},
                ]}

                categoryList={[
                    {id:'ERROR',title:'Error'},
                    {id:'IMPROVEMENT',title:'Improvement'},
                    {id:'OTHER',title:'Other'},
                ]}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        message: state.issues.message,
        error: state.issues.error,
        userId: state.user.userId,
        allUsers: state.allUsers.byIds,
        projects: state.projects.byIds,
        issues: state.issues.searchIssues
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchAllUsers: (payload) => dispatch(actions.fetchAllUsers(payload)),
        fetchProjects: (payload) => dispatch(actions.fetchProjects(payload)),
        onSubmit: (payload) => dispatch(actions.updateIssue(payload)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateHOC);