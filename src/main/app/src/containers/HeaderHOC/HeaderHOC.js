import React, {Component} from "react";
import Header from "../../components/Header/Header";
import {connect} from "react-redux";

class HeaderHOC extends Component{
    render() {
        return <Header {...this.props} />
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps)(HeaderHOC);