import React, {Component} from "react";
import Issues from "../../components/Issues/Issues";
import * as actions from "../../actions";
import {connect} from "react-redux";

class IssuesHOC extends Component{
    constructor(props){
        super(props);

        this.handleCloseSnackbar = this.handleCloseSnackbar.bind(this);
        this.handleResolve = this.handleResolve.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

    }

    handleCloseSnackbar = (event, reason) => {
        //clear message & error
        this.props.clearMessage();

        //after showing message/error refresh table values
        // in homepage dispatch searchMyIssues
        // in search page dom click in search btn
        if(this.props.location.pathname === "/search"){
            setTimeout(() => {
                if(document.getElementById('search-submit')){
                    document.getElementById('search-submit').click()
                }
            },500);
        }else if(this.props.location.pathname === "/"){
            setTimeout(() => {
                this.props.searchMyIssues({
                    sendBody: {
                        "userID": this.props.user.userId
                    }
                });
            },500);
        }
    };

    // change Status to Resolve
    handleResolve = (event, issue) => {
        event.preventDefault();

        const toBeResolved = issue;

        this.props.onResolve({
            sendBody: {
                issueId: toBeResolved.id,
                project: {
                    projectID: toBeResolved.projectId
                },
                title: toBeResolved.title,
                description: toBeResolved.description,
                assignor: {
                    userID: toBeResolved.assignorId
                },
                assignee: {
                    userID:toBeResolved.assigneeId
                },
                type: toBeResolved.type,
                status: "RESOLVED",
                otherDetails: toBeResolved.otherDetails
            }
        });
    };

    //delete Issue
    handleDelete = (event,issueId) => {
        event.preventDefault();

        this.props.onDelete({
            sendBody: {
                issueId: issueId
            }
        });
    };

    render() {
        return(
            <Issues
                {...this.props}
                handleResolve={this.handleResolve}
                handleCloseSnackbar={this.handleCloseSnackbar}
                handleDelete={this.handleDelete}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        message: state.issues.message,
        error: state.issues.error,
        issuesSearch: state.issues
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onResolve: (payload) => dispatch(actions.resolveIssue(payload)),
        clearMessage: (payload) => dispatch(actions.clearActionIssueMessage(payload)),
        onDelete: (payload) => dispatch(actions.deleteIssue(payload))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(IssuesHOC);