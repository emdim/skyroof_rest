import React from 'react';
import {Route, Switch} from "react-router";
import UpdateHOC from "../containers/UpdateHOC/UpdateHOC";
import CreateHOC from "../containers/CreateHOC/CreateHOC";
import HomepageHOC from "../containers/HomepageHOC/HomepageHOC";
import SearchHOC from "../containers/SearchHOC/SearchHOC";

const ContentRouter = (props) => {

    return (
        <Switch>
            <Route path='/update/:issueToUpdateId' component={UpdateHOC}/>
            <Route path='/search' component={SearchHOC}/>
            <Route path='/create' component={CreateHOC}/>
            <Route path="/"  component={HomepageHOC}/>
        </Switch>
    );

};

export default ContentRouter;
