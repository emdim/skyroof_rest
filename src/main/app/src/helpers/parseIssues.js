const parseIssues = (stateIssues, stateIssuesIds, data) => {
    let newIssues = stateIssues || {};
    let issuesIds = stateIssuesIds.slice() || {};

    data.forEach(item => {
        if (!item) return;

        newIssues = {
            ...newIssues,
            [item.issueId]: {
                    id: item.issueId,
                    projectId: item.project && item.project.projectID ? item.project.projectID : null,
                    projectName: item.project && item.project.name ? item.project.name : null,
                    title: item.title,
                    description: item.description,
                    assignorId: item.assignor && item.assignor.userID ? item.assignor.userID : null,
                    assignorUsername: item.assignor && item.assignor.username ? item.assignor.username : null,
                    assignorPermission: item.assignor && item.assignor.permission ? item.assignor.permission : null,
                    assigneeId: item.assignee && item.assignee.userID ? item.assignee.userID : null,
                    assigneeUsername: item.assignee && item.assignee.username ? item.assignee.username : null,
                    assigneePermission: item.assignee && item.assignee.permission ? item.assignee.permission : null,
                    type: item.type,
                    otherDetails: item.otherDetails,
                    status: item.status
            },
        };
        if (!(issuesIds).includes(item.issueId)) {
            issuesIds.push(item.issueId)
        }
    });

    return {
        newIssues,
        issuesIds
    }

};
export default parseIssues;