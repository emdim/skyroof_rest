import * as ActionTypes from '../actions/actionTypes';
import {updateObject} from "../shared/utility";
import parseIssues from "../helpers/parseIssues";

const initialState = {
    myIssues: null,
    searchIssues: null,
    byId: null,
    loading: false,
    message: null,
    error: null
};

function initSearchPage(state, action) {
    return updateObject(state, {
        searchIssues: null
    });
}

function searchIssues(state, action) {
    return updateObject(state, {
        searchIssues: null,
        message: null,
        loading: true
    });
}

function searchIssuesSuccess(state, action) {
    let parsedIssues = null;
    if (action.payload) {
        parsedIssues = parseIssues(state.searchIssues, [], action.payload);
    }
    return updateObject(state, {
        searchIssues: {
            ...state.searchIssues,
            ...parsedIssues.newIssues
        },
        message: null,
        loading: false,
    });
}

function searchIssuesFail(state, action) {
    return updateObject(state, {
        message: null,
        loading: false
    });
}

function searchMyIssues(state, action) {
    return updateObject(state, {
        searchIssues: null,
        myIssues: null,
        message: null,
        loading: true
    });
}

function searchMyIssuesSuccess(state, action) {
    let parsedIssues = null;
    if (action.payload) {
        parsedIssues = parseIssues(state.myIssues, [], action.payload);
    }
    return updateObject(state, {
        message: null,
        loading: false,
        searchIssues: {
            ...state.searchIssues,
            ...parsedIssues.newIssues
        },
        myIssues: {
            ...state.myIssues,
            ...parsedIssues.newIssues
        },
        byId: {
            ...state.byId,
            ...parsedIssues.newIssues
        },
    });
}

function searchMyIssuesFail(state, action) {
    return updateObject(state, {
        message: null,
        loading: false
    });
}


function createIssue(state, action) {
    return updateObject(state, {
        message: null,
        error: null,
        loading: true
    });
}

function createIssueSuccess(state, action) {
    return updateObject(state, {
        message: "Issue created successfully.",
        error: null,
        loading: false
    });
}

function createIssueFail(state, action) {
    return updateObject(state, {
        message: null,
        error: 'Something went wrong, while creating the issue.',
        loading: false
    });
}

function updateIssue(state, action) {
    return updateObject(state, {
        message: null,
        error: null,
        loading: true
    });
}

function updateIssueSuccess(state, action) {
    return updateObject(state, {
        message: "Issue updated successfully.",
        error: null,
        loading: false
    });
}

function updateIssueFail(state, action) {
    return updateObject(state, {
        message: null,
        error: 'Something went wrong, while updating the issue.',
        loading: false
    });
}

function resolveIssue(state, action) {
    return updateObject(state, {
        message: null,
        error: null,
        loading: true
    });
}

function resolveIssueSuccess(state, action) {
    return updateObject(state, {
        message: "Issue resolved successfully.",
        error: null,
        loading: false
    });
}

function resolveIssueFail(state, action) {
    return updateObject(state, {
        message: null,
        error: 'Something went wrong, while resolving the issue.',
        loading: false
    });
}

function clearActionIssueMessage(state, action) {
    return updateObject(state, {
        message: null,
        error:null
    });
}
function deleteIssue(state, action) {
    return updateObject(state, {
        message: null,
        error: null,
        loading: true
    });
}

function deleteIssueSuccess(state, action) {
    return updateObject(state, {
        message: "Issue deleted successfully.",
        error: null,
        loading: false
    });
}

function deleteIssueFail(state, action) {
    return updateObject(state, {
        message: null,
        error: 'Something went wrong, while deleting the issue.',
        loading: false
    });
}

export const issues = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.INIT_SEARCH_PAGE: return initSearchPage(state, action);
        case ActionTypes.SEARCH_ISSUES: return searchIssues(state, action);
        case ActionTypes.SEARCH_ISSUES_SUCCESS: return searchIssuesSuccess(state, action);
        case ActionTypes.SEARCH_ISSUES_FAIL: return searchIssuesFail(state, action);
        case ActionTypes.CREATE_ISSUE: return createIssue(state, action);
        case ActionTypes.CREATE_ISSUE_SUCCESS: return createIssueSuccess(state, action);
        case ActionTypes.CREATE_ISSUE_FAIL: return createIssueFail(state, action);
        case ActionTypes.UPDATE_ISSUE: return updateIssue(state, action);
        case ActionTypes.UPDATE_ISSUE_SUCCESS: return updateIssueSuccess(state, action);
        case ActionTypes.UPDATE_ISSUE_FAIL: return updateIssueFail(state, action);
        case ActionTypes.SEARCH_MY_ISSUES: return searchMyIssues(state, action);
        case ActionTypes.SEARCH_MY_ISSUES_FAIL: return searchMyIssuesFail(state, action);
        case ActionTypes.SEARCH_MY_ISSUES_SUCCESS: return searchMyIssuesSuccess(state, action);
        case ActionTypes.RESOLVE_ISSUE: return resolveIssue(state,action);
        case ActionTypes.RESOLVE_ISSUE_SUCCESS: return resolveIssueSuccess(state,action);
        case ActionTypes.RESOLVE_ISSUE_FAIL: return resolveIssueFail(state,action);
        case ActionTypes.CLEAR_ACTION_ISSUE_MESSAGE: return clearActionIssueMessage(state,action);
        case ActionTypes.DELETE_ISSUE: return deleteIssue(state,action);
        case ActionTypes.DELETE_ISSUE_SUCCESS: return deleteIssueSuccess(state,action);
        case ActionTypes.DELETE_ISSUE_FAIL: return deleteIssueFail(state,action);
        default:
            return state;
    }
};
