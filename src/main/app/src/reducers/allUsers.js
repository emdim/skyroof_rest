import * as ActionTypes from '../actions/actionTypes';
import { updateObject } from "../shared/utility";

const initialState = {
    byIds: null,
    loading: false,
};

function fetchAllUsers(state, action) {
    return updateObject(state, {loading: true});
}

function fetchAllUsersSuccess(state, action) {
    let allUsers = null;
    if(action.payload){
        allUsers = action.payload
    }
    return updateObject(state, {
        'loading': false,
        'byIds': allUsers
    });
}

function fetchAllUsersFail(state, action) {
    return updateObject(state, {loading: false});
}

export const allUsers = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_USERS: return fetchAllUsers(state,action);
        case ActionTypes.FETCH_USERS_SUCCESS: return fetchAllUsersSuccess(state,action);
        case ActionTypes.FETCH_USERS_FAIL: return fetchAllUsersFail(state,action);
        default:
            return state;
    }
};