import * as ActionTypes from '../actions/actionTypes';
import { updateObject } from "../shared/utility";

const initialState = {
    byIds: null,
    loading: false,
};

function fetchProjects(state, action) {
    return updateObject(state, {loading: true});
}

function fetchProjectsSuccess(state, action) {
    let projects = null;
    if(action.payload){
        projects = action.payload
    }
    return updateObject(state, {
        'loading': false,
        'byIds': projects
    });
}

function fetchProjectsFail(state, action) {
    return updateObject(state, {loading: false});
}

export const projects = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_PROJECTS: return fetchProjects(state,action);
        case ActionTypes.FETCH_PROJECTS_SUCCESS: return fetchProjectsSuccess(state,action);
        case ActionTypes.FETCH_PROJECTS_FAIL: return fetchProjectsFail(state,action);
        default:
            return state;
    }
};