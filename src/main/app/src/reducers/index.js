import {combineReducers} from "redux";
import {LOGOUT} from "../actions/actionTypes";
import storage from "redux-persist/es/storage";
import {PERSIST_KEY} from "../store/store";
import { connectRouter } from 'connected-react-router';
import {user} from "./user";
import {allUsers} from "./allUsers";
import {projects} from "./projects";
import {issues} from "./issues";

const reducerWithHistoryCreator = (history) => combineReducers({
    user: user,
    allUsers: allUsers,
    projects: projects,
    issues: issues,
    router: connectRouter(history),
});

const rootReducer = (history) => (state, action) => {
    const createdHistoryReducer = reducerWithHistoryCreator(history);

    if (action.type === LOGOUT) {
        storage.removeItem(`persist:${PERSIST_KEY}`);
        state = undefined;
    }
    return createdHistoryReducer(state, action);
};

export default rootReducer;
