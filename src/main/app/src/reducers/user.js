import * as actionTypes from '../actions/actionTypes';
import { updateObject } from "../shared/utility";

const initialState = {
    token: null,
    username: null,
    userId: null,
    permission: null,
    error: null,
    loading: false,
};

function loginStart(state,action) {
    return updateObject(state, { error: null, loading: true});
}

function loginSuccess(state,action) {
    return updateObject(state, {
        username: action.payload.loggedInUser.username,
        userId: action.payload.loggedInUser.userID,
        permission: action.payload.loggedInUser.permission,
        token: action.payload.token,
        error: null,
        loading: false
    });
}

function loginFail(state,action) {
    return updateObject(state, {
        error: 'Something went wrong, please try again.',
        loading: false
    });
}

function logout() {
    return initialState;
}

export const user = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_START: return loginStart(state,action);
        case actionTypes.LOGIN_SUCCESS: return loginSuccess(state,action);
        case actionTypes.LOGIN_FAIL: return loginFail(state,action);
        case actionTypes.LOGOUT: return logout(state,action);
        default:
            return state;
    }
};