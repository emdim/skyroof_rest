import {headersWithTokenJson} from './headers';
import xs from 'xstream';
import * as actions from "../actions";
import sampleCombine from 'xstream/extra/sampleCombine';
import * as actionTypes from "../actions/actionTypes";
import {BASE_URL} from "../helpers/constants";

const REQUEST_ALL_USERS = 'requestAllUsers';
export function fetchAllUsers(sources) {
    const state$ = sources.STATE;
    const token$ = state$.map(state => state.user.token);
    const request$ = sources.ACTION
        .filter(action => action.type === actionTypes.FETCH_USERS)
        .compose(sampleCombine(token$))
        .map(([action,token]) => {
                return ({
                    url: BASE_URL + 'api/user/get/all',
                    category: REQUEST_ALL_USERS,
                    method: 'GET',
                    headers: headersWithTokenJson(token)
                });
            }
        );

    let httpResponse$ = sources.HTTP
        .select(REQUEST_ALL_USERS)
        .map((response) => response.replaceError((err) => xs.of(err)))
        .flatten()
        .map(response => response.status === 200 ?
            actions.fetchAllUsersSuccess(response.body) :
            actions.fetchAllUsersFail(response.body));

    return {
        ACTION: httpResponse$,
        HTTP: request$
    };
}