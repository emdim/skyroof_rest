import {loginHeader} from './headers';
import xs from 'xstream';
import * as actions from "../actions";
import * as actionTypes from "../actions/actionTypes";
import {BASE_URL} from "../helpers/constants";

const REQUEST_USER_LOGIN = 'userLogin';
export function login(sources) {
    const request$ = sources.ACTION
        .filter(action => action.type === actionTypes.LOGIN_START)
        .map( action => ({
            url: BASE_URL + 'authenticate',
            category: REQUEST_USER_LOGIN,
            method: 'POST',
            headers: loginHeader(),
            send: action.payload.sendBody,
        }));

    let httpResponse$ = sources.HTTP
        .select(REQUEST_USER_LOGIN)
        .map((response) => response.replaceError((err) => xs.of(err)))
        .flatten()
        .map(response => response.status === 200 ?
            actions.loginSuccess(response.body) :
            actions.loginFail(response.body));

    return {
        ACTION: httpResponse$,
        HTTP: request$
    };
}

// redirectAfterLogout
// export function redirectAfterLogout(sources) {
//     const action$ = sources.ACTION
//         .filter(action => action.type === actionTypes.LOGOUT)
//         .map(action => push('/'));
//
//     return {
//         ACTION: action$,
//     }
// }