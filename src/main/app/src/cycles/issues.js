import {headersWithTokenJson} from './headers';
import xs from 'xstream';
import * as actions from "../actions";
import sampleCombine from 'xstream/extra/sampleCombine';
import * as actionTypes from "../actions/actionTypes";
import {BASE_URL} from "../helpers/constants";

const REQUEST_SEARCH_MY_ISSUES = 'requestSearchMyIssues';
export function searchMyIssues(sources) {
    const state$ = sources.STATE;
    const token$ = state$.map(state => state.user.token);
    const request$ = sources.ACTION
        .filter(action => action.type === actionTypes.SEARCH_MY_ISSUES)
        .compose(sampleCombine(token$))
        .map(([action,token]) => {
                return ({
                    url: BASE_URL + 'api/homepage',
                    category: REQUEST_SEARCH_MY_ISSUES,
                    method: 'POST',
                    send: action.payload.sendBody,
                    headers: headersWithTokenJson(token)
                });
            }
        );

    let httpResponse$ = sources.HTTP
        .select(REQUEST_SEARCH_MY_ISSUES)
        .map((response) => response.replaceError((err) => xs.of(err)))
        .flatten()
        .map(response => response.status === 200 ?
            actions.searchMyIssuesSuccess(response.body) :
            actions.searchMyIssuesFail(response.body));

    return {
        ACTION: httpResponse$,
        HTTP: request$
    };
}

const REQUEST_SEARCH_ISSUES = 'requestSearchIssues';
export function fetchIssues(sources) {
    const state$ = sources.STATE;
    const token$ = state$.map(state => state.user.token);
    const request$ = sources.ACTION
        .filter(action => action.type === actionTypes.SEARCH_ISSUES)
        .compose(sampleCombine(token$))
        .map(([action,token]) => {
                return ({
                    url: BASE_URL + 'api/search',
                    category: REQUEST_SEARCH_ISSUES,
                    method: 'POST',
                    send: action.payload.sendBody,
                    headers: headersWithTokenJson(token)
                });
            }
        );

    let httpResponse$ = sources.HTTP
        .select(REQUEST_SEARCH_ISSUES)
        .map((response) => response.replaceError((err) => xs.of(err)))
        .flatten()
        .map(response => response.status === 200 ?
            actions.searchIssuesSuccess(response.body) :
            actions.searchIssuesFail(response.body));

    return {
        ACTION: httpResponse$,
        HTTP: request$
    };
}

const REQUEST_CREATE_ISSUE = 'requestCreateIssue';
export function createIssue(sources) {
    const state$ = sources.STATE;
    const token$ = state$.map(state => state.user.token);
    const request$ = sources.ACTION
        .filter(action => action.type === actionTypes.CREATE_ISSUE)
        .compose(sampleCombine(token$))
        .map(([action,token]) => {
                return ({
                    url: BASE_URL + 'api/issue/save',
                    category: REQUEST_CREATE_ISSUE,
                    method: 'POST',
                    send: action.payload.sendBody,
                    headers: headersWithTokenJson(token)
                });
            }
        );

    let httpResponse$ = sources.HTTP
        .select(REQUEST_CREATE_ISSUE)
        .map((response) => response.replaceError((err) => xs.of(err)))
        .flatten()
        .map(response => response.status === 200 ?
            actions.createIssueSuccess(response.body) :
            actions.createIssueFail(response.body));

    return {
        ACTION: httpResponse$,
        HTTP: request$
    };
}

const REQUEST_UPDATE_ISSUE = 'requestUpdateIssue';
export function updateIssue(sources) {
    const state$ = sources.STATE;
    const token$ = state$.map(state => state.user.token);
    const request$ = sources.ACTION
        .filter(action => action.type === actionTypes.UPDATE_ISSUE)
        .compose(sampleCombine(token$))
        .map(([action,token]) => {
                return ({
                    url: BASE_URL + 'api/issue/update',
                    category: REQUEST_UPDATE_ISSUE,
                    method: 'PUT',
                    send: action.payload.sendBody,
                    headers: headersWithTokenJson(token)
                });
            }
        );

    let httpResponse$ = sources.HTTP
        .select(REQUEST_UPDATE_ISSUE)
        .map((response) => response.replaceError((err) => xs.of(err)))
        .flatten()
        .map(response => response.status === 200 ?
            actions.updateIssueSuccess(response.body) :
            actions.updateIssueFail(response.body));

    return {
        ACTION: httpResponse$,
        HTTP: request$
    };
}

const REQUEST_RESOLVE_ISSUE = 'requestResolveIssue';
export function resolveIssue(sources) {
    const state$ = sources.STATE;
    const token$ = state$.map(state => state.user.token);
    const request$ = sources.ACTION
        .filter(action => action.type === actionTypes.RESOLVE_ISSUE)
        .compose(sampleCombine(token$))
        .map(([action,token]) => {
                return ({
                    url: BASE_URL + 'api/issue/update',
                    category: REQUEST_RESOLVE_ISSUE,
                    method: 'PUT',
                    send: action.payload.sendBody,
                    headers: headersWithTokenJson(token)
                });
            }
        );

    let httpResponse$ = sources.HTTP
        .select(REQUEST_RESOLVE_ISSUE)
        .map((response) => response.replaceError((err) => xs.of(err)))
        .flatten()
        .map(response => response.status === 200 ?
            actions.resolveIssueSuccess(response.body) :
            actions.resolveIssueFail(response.body));

    return {
        ACTION: httpResponse$,
        HTTP: request$
    };
}

const REQUEST_DELETE_ISSUE = 'requestDeleteIssue';
export function deleteIssue(sources) {
    const state$ = sources.STATE;
    const token$ = state$.map(state => state.user.token);
    const request$ = sources.ACTION
        .filter(action => action.type === actionTypes.DELETE_ISSUE)
        .compose(sampleCombine(token$))
        .map(([action,token]) => {
                return ({
                    url: BASE_URL + 'api/issue/delete',
                    category: REQUEST_DELETE_ISSUE,
                    method: 'DELETE',
                    send: action.payload.sendBody,
                    headers: headersWithTokenJson(token)
                });
            }
        );

    let httpResponse$ = sources.HTTP
        .select(REQUEST_DELETE_ISSUE)
        .map((response) => response.replaceError((err) => xs.of(err)))
        .flatten()
        .map(response => response.status === 200 ?
            actions.deleteIssueSuccess(response.body) :
            actions.deleteIssueFail(response.body));

    return {
        ACTION: httpResponse$,
        HTTP: request$
    };
}