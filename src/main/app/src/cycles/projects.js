import {headersWithTokenJson} from './headers';
import xs from 'xstream';
import * as actions from "../actions";
import sampleCombine from 'xstream/extra/sampleCombine';
import * as actionTypes from "../actions/actionTypes";
import {BASE_URL} from "../helpers/constants";

const REQUEST_FETCH_PROJECTS = 'requestFetchProjects';
export function fetchProjects(sources) {
    const state$ = sources.STATE;
    const user$ = state$.map(state => state.user);
    const request$ = sources.ACTION
        .filter(action => action.type === actionTypes.FETCH_PROJECTS)
        .compose(sampleCombine(user$))
        .map(([action, user]) => {
                return ({
                    url: BASE_URL + 'api/project/get/all',
                    category: REQUEST_FETCH_PROJECTS,
                    method: 'GET',
                    headers: headersWithTokenJson(user.token),
                    // withCredentials: true
                });
            }
        );

    let httpResponse$ = sources.HTTP
        .select(REQUEST_FETCH_PROJECTS)
        .map((response) => response.replaceError((err) => xs.of(err)))
        .flatten()
        .map(response => response.status === 200 ?
            actions.fetchProjectsSuccess(response.body) :
            actions.fetchProjectsFail(response.body));

    return {
        ACTION: httpResponse$,
        HTTP: request$
    };
}
