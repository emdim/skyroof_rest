export const loginHeader = () => ({
    'Accept': '*/*',
    'Content-Type': 'application/json'
});

export const headersWithTokenJson = (token) => ({
    'Accept': '*/*',
    "Content-Type": "application/json",
    'Authorization': `Bearer ${token}`
});
