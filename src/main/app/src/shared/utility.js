export const actionCreator = (type) => (payload) => ({
    type,
    payload
});

export const updateObject = (oldObject, updatedProperties) => {
    return{
        ...oldObject,
        ...updatedProperties
    }
};