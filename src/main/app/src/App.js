import React from "react";
import "./App.css";
import PageLayout from "./components/PageLayout/PageLayout";
import withAuthentication from "./hocs/withAuthentication";
import {connect} from "react-redux";
import {Router} from 'react-router-dom';
import {history} from "./store/store";

function App() {
  return (
      <Router history={history}>
        <PageLayout />
      </Router>
  );
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({});

export default withAuthentication(connect(mapStateToProps, mapDispatchToProps)(App));