import {createStore, applyMiddleware} from 'redux';
import { createHashHistory as createHistory } from 'history'
import {persistStore, persistReducer, createMigrate} from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import rootReducer from '../reducers';
import {run} from '@cycle/run';
import {makeHTTPDriver} from '@cycle/http';
import {createCycleMiddleware} from 'redux-cycles';
import main from '../cycles/cycles';
import {createLogger} from 'redux-logger';
import {migrations} from "./migrations";
import { routerMiddleware } from 'connected-react-router'


const middleWare = [];
export const history = createHistory();
// middleWare.push(routerMiddleware(history));

// persist
export const PERSIST_KEY = 'rootPersist';
const config = {
    key: 'rootPersist',
    whitelist: ['user'],
    storage,
    version: 0,
    migrate: createMigrate(migrations)
};

const cycleMiddleware = createCycleMiddleware();
const {makeActionDriver, makeStateDriver} = cycleMiddleware;

middleWare.push(cycleMiddleware);

// for dispatching history actions
middleWare.push(routerMiddleware(history));

if (process.env.NODE_ENV === `development`) {
    let logger;

    logger = createLogger({
    });

    middleWare.push(logger);
}

const persistedReducer = persistReducer(config, rootReducer(history));

export const store = createStore(
    persistedReducer,
    applyMiddleware(...middleWare),
);

export const persistor = persistStore(store);

// cycles
run(main, {
    ACTION: makeActionDriver(),
    STATE: makeStateDriver(),
    HTTP: makeHTTPDriver(),
});

export default store;
