import React, {Component} from "react";
import './Issues.css';
import {CircularProgress} from "@material-ui/core";
import {Link} from "react-router-dom";
import UpdateIcon from "@material-ui/icons/Update"
import DeleteIcon from "@material-ui/icons/Delete"
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import ResolveIcon from "@material-ui/icons/PlaylistAddCheck"
import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from '@material-ui/icons/Close';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';

class Issues extends Component {
    render() {
        const {user} = this.props;

        let issuesContent = null;
        if (!this.props.loading) {
            if (this.props.issues !== null) {
                if(Object.keys(this.props.issues).length > 0) {
                    issuesContent = (
                        <div className={'issues-result'}>
                            {this.props.searchPage ?
                                <Typography variant="h5" className={'pageTitle'}>
                                    Results
                                </Typography>
                                : null}

                            <table id="issues-table">
                                <thead>
                                <tr>
                                    <th>Project</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Assignor</th>
                                    <th>Assignee</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {Object.keys(this.props.issues).map((issue, index) => (

                                    <tr key={this.props.issues[issue].id}
                                        className={[this.props.issues[issue].assignorUsername === user.username ? 'assignor' : null, this.props.issues[issue].assigneeUsername === user.username ? 'assignee' : null].join(' ')}
                                    >
                                        <td className={'issueProjectName'}>{this.props.issues[issue].projectName}</td>
                                        <td className={'issueTitle'}>{this.props.issues[issue].title}</td>
                                        <td className={'issueDescription'}>{this.props.issues[issue].description}</td>
                                        <td className={'usernameAssignor'}>{this.props.issues[issue].assignorUsername}</td>
                                        <td className={'usernameAssignee'}>{this.props.issues[issue].assigneeUsername}</td>
                                        <td className={'type'}>{this.props.issues[issue].type.toLowerCase()}</td>
                                        <td className={'status'}>{this.props.issues[issue].status.toLocaleLowerCase()}</td>
                                        <td>
                                            {/*resolve*/}
                                            {this.props.issues[issue].assigneeUsername === user.username && (this.props.issues[issue].status === 'OPEN' || this.props.issues[issue].status === "REOPEN") ?
                                                <div id={'resolve-btn'}>
                                                    <Tooltip title={'Resolve'} placement={'left'}
                                                             className={'actions-btn resolve'}>
                                                        <ResolveIcon
                                                            onClick={(event) => this.props.handleResolve(event, this.props.issues[issue])}/>
                                                    </Tooltip>
                                                </div>
                                                : null}
                                            {/*update*/}
                                            {this.props.user.permission !== "READ" ?
                                                <div id={'update-btn'}>
                                                    <Link to={"/update/" + this.props.issues[issue].id}>
                                                        <Tooltip title={'Update'}
                                                                 placement={(this.props.issues[issue].assigneeUsername === user.username && (this.props.issues[issue].status === 'OPEN' || this.props.issues[issue].status === "REOPEN")) ? 'bottom' : 'left'}
                                                                 className={'actions-btn update'}>
                                                            <UpdateIcon/>
                                                        </Tooltip>
                                                    </Link>
                                                </div>
                                                : null}
                                            {/*delete*/}
                                            {this.props.user.permission === "READ_CREATE_UPDATE_DELETE" ?
                                                <div id={'delete-btn'}>
                                                    <Tooltip title={'Delete'} placement={'right'}
                                                             className={'actions-btn delete'}>
                                                        <DeleteIcon
                                                            onClick={(event) => this.props.handleDelete(event, this.props.issues[issue].id)}/>
                                                    </Tooltip>
                                                </div>
                                                : null}
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        </div>
                    )
                }else{
                    issuesContent = (
                        <div className={'issues-result'}>
                            {this.props.searchPage ?
                                <Typography variant="h5" className={'pageTitle'}>
                                    No Results
                                </Typography>
                                : null}
                        </div>
                    );
                }
            } else {
                issuesContent = null;
            }
        } else {
            issuesContent = (
                <div className={'spinnerParent'}>
                    <CircularProgress className='spinner'/>
                </div>
            );

        }
        return (
            <>
                {(this.props.error || this.props.message)&& (
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        open={true}
                        autoHideDuration={3000}
                        onClose={this.props.handleCloseSnackbar}
                    >
                        <SnackbarContent
                            className={(this.props.error && "error") || (this.props.message && 'success')}
                            aria-describedby="client-snackbar"
                            message={
                                <span id="client-snackbar" >
                                   {(this.props.message && <CheckCircleIcon/>) || (this.props.error && <ErrorIcon/>) }
                                    {this.props.message || this.props.error}
                                </span>
                            }
                            action={[
                                <IconButton key="close" aria-label="close" color="inherit" onClick={this.props.handleCloseSnackbar}>
                                    <CloseIcon />
                                </IconButton>,
                            ]}
                        />
                    </Snackbar>
                )}
                {issuesContent}
            </>
        )
    }
}

export default Issues;
