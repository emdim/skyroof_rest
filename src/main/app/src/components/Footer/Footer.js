import React, {Component} from "react";
import './Footer.css';
import Owl from '../../assets/images/owl-logo.png';

import {Typography} from "@material-ui/core";

class Footer extends Component{
    render() {
        return(
            <footer>
                <Typography variant={"subtitle1"} align={'center'} className={'copyright'}>
                    &copy; Coding Hive 2019 <img className={'icon-owl'} src={Owl} alt={'owl'}/> #coding-night
                </Typography>
            </footer>
        )
    }
}

export default Footer;