import React, {Component} from "react";
import './Login.css';
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import {Redirect} from "react-router";


class Login extends Component {
    render() {
        let errorMessage = null;
        if (this.props.error) {
            errorMessage = (<h4 className='errorMessageForm'>{this.props.error}</h4>)
        }

        let form = (
            <div className='login-page'>
                <div className='login-bg'></div>
                <div className='login-form'>
                    <ValidatorForm onSubmit={this.props.handleSubmit} noValidate>
                        {errorMessage}
                        <TextValidator
                            onChange={this.props.handleInputChange}
                            variant="filled"
                            margin="normal"
                            required
                            fullWidth
                            value={this.props.username}
                            id="username"
                            label="Username"
                            name="username"
                            autoComplete="username"
                            autoFocus
                            validators={['required']}
                            errorMessages={['Username is required']}
                        />
                        <TextValidator
                            onChange={this.props.handleInputChange}
                            variant="filled"
                            margin="normal"
                            required
                            fullWidth
                            value={this.props.password}
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            validators={['required']}
                            errorMessages={['Password is required']}
                        />
                        <Grid container spacing={3}>
                            <Grid item xs={6}>
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className='submit'
                                    id={"login-submit"}
                                >
                                    Sign In
                                </Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className='clear'
                                    id={"login-clear"}
                                    onClick={this.props.handleClear}
                                >
                                    Clear
                                </Button>
                            </Grid>
                        </Grid>
                    </ValidatorForm>
                </div>
            </div>
        );

        if (this.props.loading) {
            form = <div className='spinnerParent'><CircularProgress className='spinner'/></div>
        }

        let authRedirect = null;
        if (this.props.isAuthenticated) {
            authRedirect = <Redirect to="/"/>
        }
        return (
            <React.Fragment>
                {authRedirect}
                {form}
            </React.Fragment>

        );
    }
}

export default Login;
