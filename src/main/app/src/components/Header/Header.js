import React, {useCallback} from 'react';
import './Header.css';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import Logout from "../Logout/Logout";
import * as actions from "../../actions";
import {useDispatch} from "react-redux";
import {Link} from "react-router-dom";

export default function Header(props) {

    const dispatch = useDispatch();
    const logoutAction = useCallback(
        (payload) => dispatch(actions.logout(payload)),
        [dispatch]
    );

    return (
        <div className={'root'}>
            <AppBar position="static" className={'headerBar'} id={'headerBar'}>
                <Toolbar>
                    <div className={'header-logo'} id={'header-logo'}>
                        <Link to="/">
                            <LocationCityIcon fontSize="large" />
                            <Typography variant="h6" className={'title'}>SkyRoof</Typography>
                        </Link>
                    </div>
                    <div className={'header-buttons'}>
                        {props.user.permission !== 'READ' ?
                            <Link to="/create">
                                <Button id={'menu-create-btn'}>Create</Button>
                            </Link>
                        :null}
                        <Link to="/search">
                            <Button id={'menu-search-btn'}>Search</Button>
                        </Link>
                    </div>
                    <div className={'header-user'} id={'header-user'}>
                        <Typography variant="subtitle1" className={'userInfo'}>Hello, {props.user.username}</Typography>
                        <Logout onClick={logoutAction}/>
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );
}