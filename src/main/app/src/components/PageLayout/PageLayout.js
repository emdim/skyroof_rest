import React   from 'react';
import HeaderHOC from "../../containers/HeaderHOC/HeaderHOC";
import ContentRouter from "../../routers/contentRouter";
import PageContentHOC from "../../containers/PageContentHOC/PageContentHOC";
import Footer from "../Footer/Footer";

const PageLayout = (props) => {

    return (
        <div>
            <HeaderHOC />
            <PageContentHOC>
                <ContentRouter/>
            </PageContentHOC>
            <Footer/>
        </div>
    )
};

export default  PageLayout;
