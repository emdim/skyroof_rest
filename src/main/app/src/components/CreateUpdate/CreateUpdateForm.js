import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { ValidatorForm, TextValidator, SelectValidator} from 'react-material-ui-form-validator';

import './CreateUpdateForm.css'
import {CircularProgress} from "@material-ui/core";

class CreateUpdateForm extends  Component {
  constructor(props){
    super(props);

    this.goBack = this.goBack.bind(this);
  }

  goBack(){
    this.props.history.goBack();
  }

  render() {
    let message = null;
    let error = null;

    if (this.props.message) {
      message = (<h3 className='successMessageForm'>{this.props.message}</h3>)
    }

    if (this.props.error) {
      error = (<h3 className='errorMessageForm'>{this.props.error}</h3>)
    }

    let renderContent;
    let homeRedirect=null;

    if(this.props.cancel || message){
      this.goBack();
    }

    if(this.props.assignorList && this.props.assigneeList && this.props.projectList
    && this.props.statusList && this.props.categoryList){
      renderContent = (
          <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className='paper'>
              {message}
              {error}
              <Typography component="h1" variant="h5">
                {this.props.pageTitle}
              </Typography>
              <ValidatorForm onSubmit={this.props.handleSubmit} noValidate>
                <Grid container spacing={2}>

                  <Grid item xs={12} id={'project-field'}>
                    <FormControl fullWidth required>
                      {/*<InputLabel className='formControl' id="demo-simple-select-label">Project Name</InputLabel>*/}
                      <SelectValidator
                          variant="outlined"
                          required
                          fullWidth
                          name="project"
                          label="Project Name"
                          id="project"
                          value={this.props.project}
                          onChange={this.props.handleInputChange}
                          validators={['required']}
                          errorMessages={['Project Name is required']}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {this.props.createMenuItem(this.props.projectList, 'projectID', 'name')}
                      </SelectValidator>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} id={'issueName-field'}>
                    <TextValidator
                        variant="outlined"
                        required
                        fullWidth
                        id="issue"
                        label="Issue Name"
                        name="issue"
                        autoComplete="issue"
                        value={this.props.issue}
                        onChange={this.props.handleInputChange}
                        validators={['required']}
                        errorMessages={['Issue Name is required']}
                    />
                  </Grid>

                  <Grid item xs={12} id={'description-field'}>
                    <TextValidator
                        variant="outlined"
                        required
                        fullWidth
                        multiline
                        rows="4"
                        rowsMax="4"
                        id="description"
                        label="Description"
                        name="description"
                        autoComplete="description"
                        value={this.props.description}
                        onChange={this.props.handleInputChange}
                        validators={['required']}
                        errorMessages={['Description is required']}
                    />
                  </Grid>

                  <Grid item xs={12} id={'assignor-field'}>
                    <FormControl fullWidth required>
                      <SelectValidator
                          variant="outlined"
                          required
                          fullWidth
                          name="assignor"
                          label="Assignor"
                          id="assignor"
                          disabled={this.props.isUpdatePage}
                          value={this.props.assignor}
                          onChange={this.props.handleInputChange}
                          validators={['required']}
                          errorMessages={['Assignor is required']}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {this.props.createMenuItem(this.props.assignorList, 'userID', 'username')}
                      </SelectValidator>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} id={'assignee-field'}>
                    <FormControl fullWidth required>
                      <SelectValidator
                          variant="outlined"
                          required
                          fullWidth
                          name="assignee"
                          label="Assignee"
                          id="assignee"
                          value={this.props.assignee}
                          onChange={this.props.handleInputChange}
                          validators={['required']}
                          errorMessages={['Assignee is required']}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {this.props.createMenuItem(this.props.assigneeList, 'userID', 'username')}
                      </SelectValidator>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} id={'status-field'}>
                    <FormControl fullWidth required>
                      <SelectValidator
                          variant="outlined"
                          required
                          fullWidth
                          name="status"
                          label="Status"
                          id="status"
                          disabled={!this.props.isUpdatePage}
                          value={this.props.status}
                          onChange={this.props.handleInputChange}
                          validators={['required']}
                          errorMessages={['Status is required']}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {this.props.createMenuItem(this.props.statusList, 'id', 'title')}
                      </SelectValidator>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} id={'category-field'}>
                    <FormControl fullWidth required>
                      <SelectValidator
                          variant="outlined"
                          required
                          fullWidth
                          name="category"
                          label="Type"
                          id="category"
                          value={this.props.category}
                          onChange={this.props.handleInputChange}
                          validators={['required']}
                          errorMessages={['Type is required']}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {this.props.createMenuItem(this.props.categoryList, 'id', 'title')}
                      </SelectValidator>
                    </FormControl>
                  </Grid>

                  <Grid item xs={12} id={'additionalInfo-field'}>
                    <TextField
                        variant="outlined"
                        fullWidth
                        multiline
                        rows="4"
                        rowsMax="4"
                        id="additionalInfo"
                        label="Additional Information"
                        name="additionalInfo"
                        autoComplete="additionalInfo"
                        value={this.props.additionalInfo}
                        onChange={this.props.handleInputChange}
                    />
                  </Grid>

                  {!this.props.isUpdatePage ? (
                      //================Create issue buttons==================
                      <>
                        <Grid item xs={6}>
                          <Button
                              fullWidth
                              variant="contained"
                              color="primary"
                              className='submit'
                              id={'create-clear'}
                              onClick={this.props.handleClear}
                          >Clear</Button>
                        </Grid>
                        <Grid item xs={6}>
                          <Button
                              type="submit"
                              fullWidth
                              variant="contained"
                              color="primary"
                              className='submit'
                              id={'create-submit'}
                          >Submit</Button>
                        </Grid>
                      </>
                  ) : (
                      //================Update issue buttons==================
                      <>
                      <Grid item xs={6}>
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            className='submit'
                            id={'update-clear'}
                            onClick={this.props.handleCancel}
                        >Cancel</Button>
                      </Grid>
                      <Grid item xs={6}>
                          <Button
                              type="submit"
                              fullWidth
                              variant="contained"
                              color="primary"
                              className='submit'
                              id={'update-submit'}
                          >Update</Button>
                      </Grid>
                      </>
                  )}

                </Grid>
              </ValidatorForm>
            </div>
          </Container>
      );
    }
    else{
      renderContent = (
        <div className={'spinnerParent'}>
          <CircularProgress className='spinner'/>
        </div>
      );
    }

    return (
        <React.Fragment>
          {homeRedirect}
          {renderContent}
        </React.Fragment>

    );
  }
}

export default CreateUpdateForm;