import React, {Component} from "react";
import './Homepage.css';
import {Container} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import IssuesHOC from "../../containers/IssuesHOC/IssuesHOC";

class Homepage extends Component{
    render() {
        return(
            <div>
                <Container>
                    <Typography variant="h4" className={'pageTitle'}>
                        My Issues
                    </Typography>
                    <IssuesHOC {...this.props} issues={this.props.myIssues} />
                </Container>
            </div>
        )
    }
}

export default Homepage;