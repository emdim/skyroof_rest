import React, {Component} from "react";
import './Search.css';
import {Container} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Filters from "../Filters/Filters";
import IssuesHOC from "../../containers/IssuesHOC/IssuesHOC";

class Search extends Component{
    render() {
        return(
            <div>
                <Container>
                    <Typography variant="h4" className={'pageTitle'}>
                        Search
                    </Typography>

                    {/*filter*/}
                    <Filters {...this.props} />

                    {/*searchResult*/}
                    <IssuesHOC
                        {...this.props}
                        issues={this.props.searchResultIssues}
                        searchPage
                    />
                </Container>
            </div>
        )
    }
}

export default Search;
