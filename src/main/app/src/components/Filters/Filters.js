import React, {Component} from "react";
import './Filter.css';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

class Filters extends Component{
    render(){
        return(
            <form onSubmit={this.props.handleSubmit} noValidate className={'filters'}>
                {/*projectName*/}
                <FormControl id={'search-project'}>
                    <InputLabel className='formControl' id="demo-simple-select-label">Project Name</InputLabel>
                    <Select
                        variant="outlined"
                        name="project"
                        label="Project Name"
                        id="project"
                        value={this.props.project}
                        onChange={this.props.handleInputChange}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {this.props.projectList? this.props.createMenuItem(this.props.projectList, 'projectID', 'name') : null}
                    </Select>
                </FormControl>

                {/*title*/}
                <TextField
                    className={'issue'}
                    variant="outlined"
                    id={'search-issue'}
                    label="Title"
                    name="issue"
                    autoComplete="issue"
                    value={this.props.issue || ''}
                    onChange={this.props.handleInputChange}
                />

                {/*assignor*/}
                <FormControl id={'search-assignor'}>
                    <InputLabel className='formControl' id="demo-simple-select-label">Assignor</InputLabel>
                    <Select
                        variant="outlined"
                        name="assignor"
                        label="Assignor"
                        id="assignor"
                        value={this.props.assignor}
                        onChange={this.props.handleInputChange}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {this.props.assignorList ? this.props.createMenuItem(this.props.assignorList, 'userID', 'username') : null}
                    </Select>
                </FormControl>

                {/*assignee*/}
                <FormControl id={'search-assignee'}>
                    <InputLabel className='formControl' id="demo-simple-select-label">Assignee</InputLabel>
                    <Select
                        variant="outlined"
                        name="assignee"
                        label="Assignee"
                        id="assignee"
                        value={this.props.assignee}
                        onChange={this.props.handleInputChange}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {this.props.assigneeList ? this.props.createMenuItem(this.props.assigneeList, 'userID', 'username') : null}
                    </Select>
                </FormControl>

                {/*status*/}
                <FormControl id={'search-status'}>
                    <InputLabel className='formControl' id="demo-simple-select-label">Status</InputLabel>
                    <Select
                        variant="outlined"
                        name="status"
                        label="Status"
                        id="status"
                        value={this.props.status}
                        onChange={this.props.handleInputChange}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {this.props.createMenuItemWithId(this.props.statusList, 'id', 'title')}
                    </Select>
                </FormControl>

                {/*type*/}
                <FormControl id={'search-type'}>
                    <InputLabel className='formControl' id="demo-simple-select-label">Type</InputLabel>
                    <Select
                        variant="outlined"
                        name="type"
                        label="Type"
                        id="category"
                        value={this.props.type}
                        onChange={this.props.handleInputChange}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {this.props.typeList ? this.props.createMenuItemWithId(this.props.typeList, 'id', 'title') : null}
                    </Select>
                </FormControl>

                <div className={'btns-custom-search'}>
                    <Button color="secondary" className={'search-MyIssues'} id={'search-myIssues'} onClick={this.props.handleSearchMyIssues} >
                       My Issues
                    </Button>
                    <Button color="secondary" className={'search-AllOpenIssues'} id={'search-allOpenIssues'} onClick={this.props.handleSearchOpenIssues}>
                       Open Issues
                    </Button>
                </div>

                <div className={'search-btns'}>
                    {/*btn - get MY open issues*/}
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        className='submit'
                        id={'search-submit'}
                    >Search</Button>

                    {/*btn - get open issues*/}
                    <Button
                        variant="contained"
                        color="primary"
                        className='submit'
                        id={'search-clear'}
                        onClick={this.props.handleClear}
                    >Clear</Button>
                </div>
            </form>
        )
    }
}

export default Filters;