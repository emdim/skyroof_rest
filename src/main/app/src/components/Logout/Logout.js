import React from 'react';
import './Logout.css';
import ExitToApp from "@material-ui/icons/ExitToApp";
import Grid from "@material-ui/core/Grid";
import * as actions from "../../actions";
import {connect} from "react-redux";

const Logout = (props) => {

    return (
        <div className="logout">
            <Grid
                container spacing={1}
                direction="row"
                justify="center"
                alignItems="center"
            >
                <Grid item xs={12}>
                    <ExitToApp onClick={ ()=>props.onLogout() }/>
                </Grid>
            </Grid>
        </div>
    );

};

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch(actions.logout())
    }
};

export default connect(null,mapDispatchToProps)(Logout);
