import React from "react";
import LoginHOC from "../containers/LoginHOC/LoginHOC";
import {connect} from "react-redux";
const withAuthentication = (WrappedComponent) => {
    class LoginAuthHOC extends React.Component{
        render(){
            const {token} = this.props;

            if(!token){
                return (
                    <LoginHOC/>
                )}

            return (
                <WrappedComponent {...this.props} />
            )
        }
    }

    const mapStateToProps = (state) => ({
        token: state.user.token
    });

    const mapDispatchToProps = (dispatch) => ({});
    return connect(mapStateToProps, mapDispatchToProps)(LoginAuthHOC)
};


export default withAuthentication;
