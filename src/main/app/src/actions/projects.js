import * as ActionTypes from './actionTypes';
import {actionCreator} from "../shared/utility";

export const fetchProjects = actionCreator(ActionTypes.FETCH_PROJECTS);
export const fetchProjectsSuccess = actionCreator(ActionTypes.FETCH_PROJECTS_SUCCESS);
export const fetchProjectsFail= actionCreator(ActionTypes.FETCH_PROJECTS_FAIL);