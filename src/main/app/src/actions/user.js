import * as ActionTypes from './actionTypes';
import {actionCreator} from "../shared/utility";

export const loginStart = actionCreator(ActionTypes.LOGIN_START);
export const loginSuccess = actionCreator(ActionTypes.LOGIN_SUCCESS);
export const loginFail = actionCreator(ActionTypes.LOGIN_FAIL);

export const logout = actionCreator(ActionTypes.LOGOUT);

