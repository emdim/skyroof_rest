import * as ActionTypes from './actionTypes';
import {actionCreator} from "../shared/utility";

// SEARCH_MY_ISSUES
export const searchMyIssues = actionCreator(ActionTypes.SEARCH_MY_ISSUES);
export const searchMyIssuesSuccess = actionCreator(ActionTypes.SEARCH_MY_ISSUES_SUCCESS);
export const searchMyIssuesFail= actionCreator(ActionTypes.SEARCH_MY_ISSUES_FAIL);

//SEARCH_ISSUES
export const searchIssues = actionCreator(ActionTypes.SEARCH_ISSUES);
export const searchIssuesSuccess = actionCreator(ActionTypes.SEARCH_ISSUES_SUCCESS);
export const searchIssuesFail= actionCreator(ActionTypes.SEARCH_ISSUES_FAIL);

//CREATE_ISSUES
export const createIssue = actionCreator(ActionTypes.CREATE_ISSUE);
export const createIssueSuccess = actionCreator(ActionTypes.CREATE_ISSUE_SUCCESS);
export const createIssueFail = actionCreator(ActionTypes.CREATE_ISSUE_FAIL);

//UPDATE_ISSUES
export const updateIssue = actionCreator(ActionTypes.UPDATE_ISSUE);
export const updateIssueSuccess = actionCreator(ActionTypes.UPDATE_ISSUE_SUCCESS);
export const updateIssueFail = actionCreator(ActionTypes.UPDATE_ISSUE_FAIL);

//INIT_SEARCH
export const initSearchPage = actionCreator(ActionTypes.INIT_SEARCH_PAGE);

//RESOLVE_ISSUE
export const resolveIssue = actionCreator(ActionTypes.RESOLVE_ISSUE);
export const resolveIssueSuccess = actionCreator(ActionTypes.RESOLVE_ISSUE_SUCCESS);
export const resolveIssueFail= actionCreator(ActionTypes.RESOLVE_ISSUE_FAIL);
export const clearActionIssueMessage= actionCreator(ActionTypes.CLEAR_ACTION_ISSUE_MESSAGE);

//DELETE_ISSUE
export const deleteIssue = actionCreator(ActionTypes.DELETE_ISSUE);
export const deleteIssueSuccess = actionCreator(ActionTypes.DELETE_ISSUE_SUCCESS);
export const deleteIssueFail= actionCreator(ActionTypes.DELETE_ISSUE_FAIL);