import * as ActionTypes from './actionTypes';
import {actionCreator} from "../shared/utility";

export const fetchAllUsers = actionCreator(ActionTypes.FETCH_USERS);
export const fetchAllUsersSuccess = actionCreator(ActionTypes.FETCH_USERS_SUCCESS);
export const fetchAllUsersFail= actionCreator(ActionTypes.FETCH_USERS_FAIL);
