package com.codingnight.skyroof.dao;

import com.codingnight.skyroof.model.Project;
import com.codingnight.skyroof.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ProjectDao implements Dao<Project, Long> {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Optional<Project> get(Long id) {
        return projectRepository.findById(id);
    }

    @Override
    public Iterable<Project> getAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project save(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public void delete(Project project) {
        projectRepository.delete(project);
    }

    @Override
    public Project update(Project project) {
        return projectRepository.save(project);
    }

    public List<Project> getProjectByName(Project project){ return projectRepository.findProjectByName(project.getName()); }

    public List<Project> getAllProjectsByName(Project project) {
        return projectRepository.findProjectByName(project.getName());
    }
}
