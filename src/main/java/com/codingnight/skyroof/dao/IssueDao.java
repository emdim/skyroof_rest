package com.codingnight.skyroof.dao;

import com.codingnight.skyroof.model.*;
import com.codingnight.skyroof.repository.IssueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Optional;

@Component
public class IssueDao implements Dao<Issue, Long> {

    @Autowired
    private IssueRepository issueRepository;

    @Override
    public Optional<Issue> get(Long id) {
        return issueRepository.findById(id);
    }

    @Override
    public Iterable<Issue> getAll() {
        return issueRepository.findAll();
    }

    @Override
    public Issue save(Issue issue) {
        return issueRepository.save(issue);
    }

    @Override
    public void delete(Issue issue) {
        issueRepository.delete(issue);
    }

    @Override
    public Issue update(Issue issue) {

        return issueRepository.save(issue);
    }

    public List<User> getAssignors(User user) {
        return issueRepository.findAssignorsByUserId(user);
    }

    public List<Issue> getEnrolledIssues(User user){ return  issueRepository.findEnrolledIssuesByIssueId(user); }

    public List<Issue> getIssuesBySearchModel(SearchModel searchModel) {
        if (searchModel.getType() != null && searchModel.getStatus() != null ){
            return issueRepository.findIssuesByAssignorAssigneeProjectStatusType(searchModel.getTitle(),
                    searchModel.getAssignor().getUsername(),
                    searchModel.getAssignee().getUsername(),
                    searchModel.getProject().getName(),
                    searchModel.getType(),
                    searchModel.getStatus());
        }
        if (searchModel.getStatus() != null ) {
            return issueRepository.findIssuesByAssignorAssigneeProjectStatus(searchModel.getTitle(),
                    searchModel.getAssignor().getUsername(),
                    searchModel.getAssignee().getUsername(),
                    searchModel.getProject().getName(),
                    searchModel.getStatus());
        }
        if (searchModel.getType() != null ) {
            return issueRepository.findIssuesByAssignorAssigneeProjectType(searchModel.getTitle(),
                    searchModel.getAssignor().getUsername(),
                    searchModel.getAssignee().getUsername(),
                    searchModel.getProject().getName(),
                    searchModel.getType());
        }


        return issueRepository.findIssuesByAssignorAssigneeProject(searchModel.getTitle(),
                searchModel.getAssignor().getUsername(),
                searchModel.getAssignee().getUsername(),
                searchModel.getProject().getName());
    }
}

