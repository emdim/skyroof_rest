package com.codingnight.skyroof.dao;

import java.util.Optional;

public interface Dao<T1, T2> {
    Optional<T1> get(T2 id);

    Iterable<T1> getAll();

    T1 save(T1 t);

    void delete(T1 t);

    T1 update(T1 t);

}
