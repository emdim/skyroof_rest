package com.codingnight.skyroof.dao;

import com.codingnight.skyroof.model.PermissionIDEnum;
import com.codingnight.skyroof.model.User;
import com.codingnight.skyroof.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserDao implements Dao<User, Long> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<User> get(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public Iterable<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

    public User getUserByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public List<User> getUserByUser(User user){
        return userRepository.findByUser(user);
    }

    public User getUserByUsernamePassword(String u, String p) {
        return userRepository.findUserByUsernameAndPassword(u,p);
    }

    public List<User> getAssignorsByUsernameAndPermission(List<User> userList){
        return userRepository.findAssignorsByUsernameAndPermission(userList.get(0),  userList.get(1).getUsername(), PermissionIDEnum.READ);
    }

    public List<User> getAssigneesByUsername(User user) {
        return userRepository.findAssigneesByUsername(user.getUsername());
    }

}
