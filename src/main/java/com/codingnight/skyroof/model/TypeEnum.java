package com.codingnight.skyroof.model;

public enum TypeEnum {
    ERROR,
    IMPROVEMENT,
    OTHER
}
