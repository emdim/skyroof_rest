package com.codingnight.skyroof.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userID")
    private Long userID;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(name = "permission")
    private PermissionIDEnum permission;

    @Column(name = "email")
    private String email;

    public User(){}

    public Long getUserID() {
        return userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PermissionIDEnum getPermission() {
        return permission;
    }

    public void setPermission(PermissionIDEnum permission) {
        this.permission= permission;/*
        if (permission == "READ")
            this.permission = PermissionIDEnum.READ;
        else if (permission == "READ_CREATE_UPDATE")
            this.permission = PermissionIDEnum.READ_CREATE_UPDATE;
        else
            this.permission = PermissionIDEnum.READ_CREATE_UPDATE_DELETE;*/


    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "userID=" + userID +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", permission=" + permission +
                '}';
    }
}
