package com.codingnight.skyroof.model;

public enum PermissionIDEnum {
        READ,
        READ_CREATE_UPDATE,
        READ_CREATE_UPDATE_DELETE;
}
