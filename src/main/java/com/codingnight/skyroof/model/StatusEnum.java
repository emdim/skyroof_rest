package com.codingnight.skyroof.model;

public enum StatusEnum {
    OPEN,
    REOPEN,
    RESOLVED,
    CLOSED,
    DELETED
}