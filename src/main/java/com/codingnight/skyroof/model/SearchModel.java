package com.codingnight.skyroof.model;

public class SearchModel {
    private User assignor;
    private User assignee;
    private Project project;
    private StatusEnum status;
    private TypeEnum type;
    private String title;

    public SearchModel(){}

    public SearchModel(User assignor, User assignee, Project project, StatusEnum status, TypeEnum type, String title) {
        this.assignor = assignor;
        this.assignee = assignee;
        this.project = project;
        this.status = status;
        this.type = type;
        this.title = title;
    }

    public User getAssignor() {
        return assignor;
    }

    public void setAssignor(User assignor) {
        this.assignor = assignor;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
