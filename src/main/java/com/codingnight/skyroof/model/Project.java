package com.codingnight.skyroof.model;
import javax.persistence.*;

@Entity
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "projectID")
    private Long projectID;

    @Column(name = "name")
    private String name;

    public Project(){}

    public Long getProjectID() {
        return projectID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Project{" +
                "projectId=" + projectID+
                ", name='" + name + '\'' +
                '}';
    }
}
