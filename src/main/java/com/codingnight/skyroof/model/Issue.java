package com.codingnight.skyroof.model;


import javax.persistence.*;

@Entity
@Table(name  = "issue")
public class Issue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "issueID")
    private Long issueId;

    @ManyToOne
    @JoinColumn(name = "projectID",referencedColumnName = "projectID")
    private Project project;


   //@Column(name = "title")
    private String title;

   //@Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "assignor", referencedColumnName = "userID")
    private User assignor;

    @ManyToOne
    @JoinColumn(name = "assignee", referencedColumnName = "userID")
    private User assignee;

    @Column(name = "type")
    private TypeEnum type;

    @Column(name = "other_details", columnDefinition = "text")
    private String otherDetails;

    @Column(name = "status")
    private StatusEnum status;


    public Long getIssueId() {
        return issueId;
    }


    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAssignor() {
        return assignor;
    }

    public void setAssignor(User assignor) {
        this.assignor = assignor;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public  Issue(){}
    @Override
    public String toString() {
        return "Issue{" +
                "issueId=" + issueId +
                ", project=" + project +
                ", title='" + title + '\'' +
                ", description=" + description +
                ", assignor=" + assignor +
                ", assignee=" + assignee +
                ", type=" + type +
                ", otherDetails='" + otherDetails + '\'' +
                '}';
    }
}
