package com.codingnight.skyroof.repository;

import com.codingnight.skyroof.model.Issue;
import com.codingnight.skyroof.model.Project;
import com.codingnight.skyroof.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;


@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

    @Query("SELECT p FROM Project p WHERE UPPER(p.name) LIKE CONCAT(UPPER(:name),'%')")
    List<Project> findProjectByName(@Param("name") String name);

}
