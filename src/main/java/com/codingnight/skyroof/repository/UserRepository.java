package com.codingnight.skyroof.repository;

import com.codingnight.skyroof.model.PermissionIDEnum;
import com.codingnight.skyroof.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE (u.username)  = :username")
    User findByUsername(@Param("username") String username);

    @Query("SELECT u FROM User u WHERE UPPER(u) LIKE CONCAT(:user,'%')")
    List<User> findByUser(@Param("user") User user);

    @Query("SELECT u FROM User u WHERE u.username=:username AND u.password=:password")
    User findUserByUsernameAndPassword(@Param("username") String username, @Param("password") String password);

    @Query("SELECT u FROM User u WHERE UPPER(u.username) LIKE CONCAT(UPPER(:username),'%') AND u <> :user AND u.permission <> :permission")
    List<User> findAssignorsByUsernameAndPermission(@Param("user")User user, @Param("username") String username,@Param("permission") PermissionIDEnum permission);

    @Query("SELECT u FROM User u WHERE UPPER(u.username) LIKE CONCAT(UPPER(:username),'%') ")
    List<User> findAssigneesByUsername(@Param("username") String username);
}
