package com.codingnight.skyroof.repository;

import com.codingnight.skyroof.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.awt.*;
import java.util.List;
import java.util.UUID;

@Repository
public interface IssueRepository extends CrudRepository<Issue, Long> {


    @Query("SELECT i.assignor FROM Issue i WHERE i.assignor <> :user")
    List<User> findAssignorsByUserId(@Param("user") User user);

    @Query("SELECT i FROM Issue i WHERE " +
            "(UPPER(i.assignor.username) LIKE CONCAT(UPPER(:assignor),'%'))" +
            "AND (UPPER(i.title) LIKE CONCAT(UPPER(:title),'%'))" +
            "AND (UPPER(i.assignee.username) LIKE CONCAT(UPPER(:assignee),'%'))" +
            "AND (UPPER(i.project.name) LIKE CONCAT(UPPER(:project),'%'))" +
            "AND (i.status = :status )")
    List<Issue> findIssuesByAssignorAssigneeProjectStatus(@Param("title") String title,
                                                          @Param("assignor") String assignor,
                                                           @Param("assignee") String assignee,
                                                           @Param("project") String project,
                                                           @Param("status") StatusEnum status);


    @Query("SELECT i FROM Issue i WHERE " +
            "(UPPER(i.assignor.username) LIKE CONCAT(UPPER(:assignor),'%'))" +
            "AND (UPPER(i.title) LIKE CONCAT(UPPER(:title),'%'))" +
            "AND (UPPER(i.assignee.username) LIKE CONCAT(UPPER(:assignee),'%'))" +
            "AND (UPPER(i.project.name) LIKE CONCAT(UPPER(:project),'%'))" +
            "AND ((i.type) = :type)" +
            "AND (i.status = :status AND i.status <4 )")
    List<Issue> findIssuesByAssignorAssigneeProjectStatusType(@Param("title") String title,
                                                              @Param("assignor") String assignor,
                                                              @Param("assignee") String assignee,
                                                              @Param("project") String project,
                                                              @Param("type") TypeEnum typeEnum,
                                                              @Param("status") StatusEnum statusEnum);


    @Query("SELECT i FROM Issue i WHERE " +
            "(UPPER(i.assignor.username) LIKE CONCAT(UPPER(:assignor),'%'))" +
            "AND (UPPER(i.title) LIKE CONCAT(UPPER(:title),'%'))" +
            "AND (UPPER(i.assignee.username) LIKE CONCAT(UPPER(:assignee),'%'))" +
            "AND (UPPER(i.project.name) LIKE CONCAT(UPPER(:project),'%'))" +
            "AND (i.status < 4)")
    List<Issue> findIssuesByAssignorAssigneeProject(@Param("title") String title,
                                                    @Param("assignor") String assignor,
                                                    @Param("assignee") String assignee,
                                                    @Param("project") String project);

    @Query("SELECT i FROM Issue i WHERE (i.assignor = :id OR i.assignee = :id) AND i.status<4")
    List<Issue> findEnrolledIssuesByIssueId(@Param("id") User id);

    @Query("SELECT i FROM Issue i WHERE " +
            "(UPPER(i.assignor.username) LIKE CONCAT(UPPER(:assignor),'%'))" +
            "AND (UPPER(i.title) LIKE CONCAT(UPPER(:title),'%'))" +
            "AND (UPPER(i.assignee.username) LIKE CONCAT(UPPER(:assignee),'%'))" +
            "AND (UPPER(i.project.name) LIKE CONCAT(UPPER(:project),'%'))" +
            "AND ((i.type) = :type)" +
            "AND (i.status <4 )")
    List<Issue> findIssuesByAssignorAssigneeProjectType(@Param("title") String title,
                                                        @Param("assignor") String assignor,
                                                        @Param("assignee") String assignee,
                                                        @Param("project") String project,
                                                        @Param("type") TypeEnum typeEnum);
}
