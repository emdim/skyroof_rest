package com.codingnight.skyroof.jwtsecurity.model;

import com.codingnight.skyroof.model.User;

import java.io.Serializable;

public class JwtResponse implements Serializable {
    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwtToken;
    private final User loggedInUser;

    public JwtResponse(String jwtToken, User loggedInUser) {
        this.jwtToken = jwtToken;
        this.loggedInUser = loggedInUser;
    }

    public String getToken() {
        return this.jwtToken;
    }

    public User getLoggedInUser() { return loggedInUser; }


}
