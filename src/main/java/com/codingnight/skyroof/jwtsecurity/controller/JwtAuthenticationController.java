package com.codingnight.skyroof.jwtsecurity.controller;


import com.codingnight.skyroof.dao.UserDao;
import com.codingnight.skyroof.jwtsecurity.config.JwtTokenUtil;
import com.codingnight.skyroof.jwtsecurity.model.JwtRequest;
import com.codingnight.skyroof.jwtsecurity.model.JwtResponse;
import com.codingnight.skyroof.jwtsecurity.service.JwtUserDetailsService;
import com.codingnight.skyroof.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) {

        try {
            authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Bad credentials");
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        User currentUser = userDao.getUserByUsername(authenticationRequest.getUsername());
        if (currentUser == null) {
            return ResponseEntity.badRequest().body(
                    "User not found with username: " + authenticationRequest.getUsername());
            //throw new UsernameNotFoundException("User not found with username: " + authenticationRequest.getUsername());
        }

        return ResponseEntity.ok(new JwtResponse(token, currentUser));
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> saveUser(@RequestBody User user) throws Exception {
        try {
            if (user.getPermission() == null || user.getUsername() == null || user.getPassword() == null || user.getEmail() == null)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A necessary field is empty check again!");
            if (user.getUsername().length() == 0 || user.getPassword().length() == 0 || user.getEmail().length() == 0)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Field is Empty!");
            if (userDao.getUserByUsername(user.getUsername()) != null)
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");

            return ResponseEntity.ok(userDetailsService.save(user));
        }catch (DataIntegrityViolationException exc) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Check your fields", exc);
        }
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
