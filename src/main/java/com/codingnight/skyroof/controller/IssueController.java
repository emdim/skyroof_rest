package com.codingnight.skyroof.controller;

import com.codingnight.skyroof.dao.IssueDao;
import com.codingnight.skyroof.model.StatusEnum;
import com.codingnight.skyroof.model.Issue;
import com.codingnight.skyroof.model.StatusEnum;
import com.codingnight.skyroof.model.TypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class IssueController implements CRUDController<Issue, Long> {

    @Autowired
    private IssueDao issueDao;

    @Override
    @GetMapping(path = "/issue/get/all")
    public Collection<Issue> getAll() {

        return (List<Issue>) issueDao.getAll();
    }

    @Override
    @GetMapping(path = "/issue/get/id/{id}")
    public Issue getById(@PathVariable Long id) {
        try{
            if(id==null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID is null");
            return issueDao.get(id).get();
        }catch (NoSuchElementException nsee){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no Issue with such ID");
        }
    }

    @Override
    @PostMapping(path = "/issue/save")
    public Issue save(@RequestBody Issue issue) {
        try {
            if (issue.getAssignor() == null || issue.getAssignee() == null || issue.getTitle() == null || issue.getProject() == null || issue.getStatus() == null || issue.getType() == null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A necessary field is null check again!");
            if(issue.getProject().getProjectID() == null || issue.getAssignee().getUserID() == null || issue.getAssignor().getUserID() == null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "An ID in assignor or assignee or project is missing!");
            if (issue.getTitle().length() == 0 || issue.getDescription().length()==0 ) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Title or description is empty!");

            return issueDao.save(issue);
        }catch (DataIntegrityViolationException exc) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Check your fields again", exc);
        }
    }

    @Override
    @DeleteMapping("/issue/delete")
    public void delete(@RequestBody Issue issue) {
        Issue newIssue = this.getById(issue.getIssueId());
        newIssue.setStatus(StatusEnum.DELETED);
        issueDao.save(newIssue);
    }

    @Override
    @PutMapping (path = "/issue/update")
    public Issue update(@RequestBody Issue issue) {

        try {
            if (issue.getAssignor() == null || issue.getAssignee() == null || issue.getTitle() == null || issue.getProject() == null || issue.getStatus() == null || issue.getType() == null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A necessary field is null check again!");
            if(issue.getProject().getProjectID() == null || issue.getAssignee().getUserID() == null || issue.getAssignor().getUserID() == null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "An ID in assignor or assignee or project is missing!");
            if (issue.getTitle().length() == 0 || issue.getDescription().length()==0 ) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Title or description is empty!");

            return issueDao.update(issue);
        }catch (DataIntegrityViolationException exc) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Check your fields again", exc);
        }
    }
}
