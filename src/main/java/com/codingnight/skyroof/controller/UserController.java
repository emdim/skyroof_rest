package com.codingnight.skyroof.controller;

import com.codingnight.skyroof.dao.UserDao;
import com.codingnight.skyroof.model.User;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolationException;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class UserController implements CRUDController<User,Long>{

    @Autowired // This means to get the bean called userRepository
    private UserDao userDao;// Which is auto-generated by Spring, we will use it to handle the data

    @Override
    @GetMapping(path = "/user/get/all")
    public Collection<User> getAll() {

        return (List<User>) userDao.getAll();

    }

    @Override
    @GetMapping(path = "/user/get/id/{id}")
    public User getById(@PathVariable Long id) {

        try{
            if(id==null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID is null");
            return userDao.get(id).get();
        }catch (NoSuchElementException nsee){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no User with such ID");
        }

    }

    @Override
    @PostMapping(path = "/user/save")
    public User save(@RequestBody User user) {

        try {
            if (user.getPermission() == null || user.getUsername() == null || user.getPassword() == null || user.getEmail() == null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A necessary field is empty check again!");
            if (user.getUsername().length() == 0 || user.getPassword().length() == 0 || user.getEmail().length() ==0  ) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Field is Empty!");
            if (userDao.getUserByUsername(user.getUsername()) != null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
            return userDao.save(user);
        }catch (DataIntegrityViolationException exc) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Check your fields", exc);
        }
    }

    @Override
    @DeleteMapping("/user/delete")
    public void delete(User user) {

    }

    @Override
    @PutMapping(path = "/user/update")
    public User update(@RequestBody User user) {
        try {
            if (user.getUserID()==null ) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "A necessary field is empty check again!");
            return userDao.update(user);
        }catch (DataIntegrityViolationException exc) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Check your fields", exc);
        }
    }



}
