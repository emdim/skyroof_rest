package com.codingnight.skyroof.controller;

import com.codingnight.skyroof.dao.IssueDao;
import com.codingnight.skyroof.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;

@RestController
@RequestMapping(path = "/api")
@CrossOrigin(origins = "http://localhost:3000")
public class SearchController {
    @Autowired
    private IssueDao issueDao;

    @PostMapping(path = "/search")
    public @ResponseBody
    List<Issue> getSearchFilterResults(@RequestBody SearchModel searchModel)
    {
        // Making sure no nulls come to our way -> Error pops up
        if (searchModel.getTitle() == null){
            searchModel.setTitle(new String());
        }
        if (searchModel.getAssignor().getUsername() == null){
            searchModel.setAssignor(new User());
            searchModel.getAssignor().setUsername(new String());
        }
        if (searchModel.getAssignee().getUsername() == null)
        {
            searchModel.setAssignee(new User());
            searchModel.getAssignee().setUsername(new String());
        }
        if (searchModel.getProject().getName() == null)
        {
            searchModel.setProject(new Project());
            searchModel.getProject().setName(new String());
        }

        return  issueDao.getIssuesBySearchModel(searchModel);
    }

    @GetMapping(path = "/search")
    public @ResponseBody
    SearchModel getSearchFilterResult()
    {
        // Making sure no nulls come to our way -> Error pops up
        SearchModel searchModel = new SearchModel();
        if (searchModel == null){
            searchModel = new SearchModel();
        }
        if (searchModel.getTitle() == null){
            searchModel.setTitle(new String());
        }
        if (searchModel.getAssignor() == null){
            searchModel.setAssignor(new User());
            searchModel.getAssignor().setUsername(new String());
        }
        if (searchModel.getAssignee() == null)
        {
            searchModel.setAssignee(new User());
            searchModel.getAssignee().setUsername(new String());
        }
        if (searchModel.getProject() == null)
        {
            searchModel.setProject(new Project());
            searchModel.getProject().setName(new String());
        }
        return searchModel;
    }

    @PostMapping(path = "/homepage")
    public @ResponseBody
    List<Issue> getEnrolledIssues(@RequestBody User user){
        return issueDao.getEnrolledIssues(user);
    }


}
