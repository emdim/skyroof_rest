package com.codingnight.skyroof.controller;

import com.codingnight.skyroof.dao.ProjectDao;
import com.codingnight.skyroof.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ProjectController implements CRUDController<Project, Long>  {

    @Autowired
    private ProjectDao projectDao;


    @Override
    @GetMapping(path = "/project/get/all")
    public Collection<Project> getAll() {

            return (List<Project>) projectDao.getAll();
    }

    @Override
    @GetMapping(path = "/project/get/id/{id}")
    public Project getById(@PathVariable Long id) {
        try{
            if(id==null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID is null");
            return projectDao.get(id).get();
        }catch (NoSuchElementException nsee){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no Project with such ID");
        }
    }

    @Override
    @PostMapping(path = "/project/save")
    public Project save(@RequestBody Project project) {

        try {
            if (project.getName() == null ) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Field name is set null");
            if(project.getName().length() == 0) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Field name is empty");

            return projectDao.save(project);
        }catch (DataIntegrityViolationException exc) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Check your fields again", exc);
        }

    }

    @Override
    @DeleteMapping("/project/delete")
    public void delete(Project project) {

    }

    @Override
    @PutMapping(path = "/project/update")
    public Project update(@RequestBody Project project) {
        try {
            if (project.getProjectID() == null || project.getName() == null ) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Field name or ID is set null");
            if(project.getName().length() == 0) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Field name is empty");

            return projectDao.update(project);
        }catch (DataIntegrityViolationException exc) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Check your fields again", exc);
        }
    }

    @GetMapping(path = "/project/get/name")
    public @ResponseBody
    List<String> getProjectsName() {
        Iterable<Project> it = projectDao.getAll();

        List<Project> result = new ArrayList<>();
        it.forEach(result::add);
        List<String> name_res = new ArrayList<>();

        for (int i = 0;i<result.size();i++){
            name_res.add(result.get(i).getName());
        }
        return name_res;
    }



}
