package com.codingnight.skyroof.controller;

import com.codingnight.skyroof.dao.IssueDao;
import com.codingnight.skyroof.dao.ProjectDao;
import com.codingnight.skyroof.dao.UserDao;
import com.codingnight.skyroof.model.Issue;
import com.codingnight.skyroof.model.Project;
import com.codingnight.skyroof.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/api")
@CrossOrigin(origins = "http://localhost:3000")
public class AutocompleteController {
    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private UserDao userDao;


    @PostMapping(path = "/autocomplete/assignor/other")
    public @ResponseBody
    List<User> getOtherAssignors(@RequestBody List<User> user) {
        return userDao.getAssignorsByUsernameAndPermission(user);
    }

    @PostMapping(path = "/autocomplete/assignee")
    public @ResponseBody
    List<User> getAllAssignees(@RequestBody User user){
        return userDao.getAssigneesByUsername(user);
    }

    @PostMapping(path = "/autocomplete/project")
    public @ResponseBody
    List<Project> getAllProjectsByName(@RequestBody Project project){
        return projectDao.getAllProjectsByName(project);
    }

    @PostMapping(path = "/autocomplete/assignor")
    public @ResponseBody
    User getLoginAssignorName(@RequestBody User user){
        return user;
    }

    @GetMapping(path = "/autocomplete/type")
    public List<String> getAllTypes() {
        List<String> listType = new ArrayList<>();
        listType.add("ERROR");
        listType.add("IMPROVEMENT");
        listType.add("OTHER");
        return listType;
    }


}
