package com.codingnight.skyroof.controller;

import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequestMapping(path = "/api")
public interface CRUDController<T1, T2> {

    @GetMapping(path = "/getAll")
    @ResponseBody
    Collection<T1> getAll();

    @GetMapping(path = "/getById")
    @ResponseBody
    T1 getById(T2 id);

    @PostMapping(path = "/save")
    @ResponseBody
    T1 save(T1 obj);

    @DeleteMapping("/delete")
    @ResponseBody
    void delete(T1 obj);

    @PutMapping("/update")
    @ResponseBody
    T1 update(T1 obj);

}