package com.codingnight.skyroof;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkyroofApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkyroofApplication.class, args);
	}

}
