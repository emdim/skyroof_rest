-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2019 at 12:38 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skyroof5`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(2),
(2),
(2);

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

CREATE TABLE `issue` (
  `issueID` bigint(20) NOT NULL,
  `projectID` bigint(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `assignor` bigint(20) NOT NULL,
  `assignee` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  `other_details` text DEFAULT NULL,
  `status` int(11) NOT NULL,
  `text` int(11) DEFAULT NULL
) ;

--
-- Dumping data for table `issue`
--

INSERT INTO `issue` (`issueID`, `projectID`, `title`, `description`, `assignor`, `assignee`, `type`, `other_details`, `status`, `text`) VALUES
(1, 1, 'tt', '2', 1, 1, 2, 'gj', 0, NULL),
(7, 1, 'BAD ARCHITECTURE', 'The design of the building is not according to plans', 1, 4, 1, 'n/a', 2, NULL),
(10, 1, 'BAD ARCHITECTURE', 'plans', 1, 4, 1, 'n/a', 2, NULL),
(15, 1, 'marika', '3', 1, 2, 1, 'yyyyy', 0, NULL),
(22, 1, 'HJJU', '4', 2, 1, 0, 'KUG', 0, NULL),
(35, 1, 'TEST', 'NONE', 2, 2, 1, 'LOL', 3, NULL),
(36, 1, 'TEST', 'WHY', 2, 2, 1, 'LOL', 3, NULL),
(87, 1, 'jjj', '2', 2, 1, 0, 'jy', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `name` varchar(50) DEFAULT NULL,
  `projectID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`name`, `projectID`) VALUES
('jj', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID` bigint(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `permission` int(11) NOT NULL
) ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `username`, `password`, `email`, `permission`) VALUES
(1, 'jj', 'jj', 'jj', 1),
(2, 'kk', 'kk', 'kk', 0),
(3, 'mm', 'mm', 'mm', 2),
(4, 'ii', 'ii', 'ii', 2),
(5, 'iiiii', 'iiii', 'ttt', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `issue`
--
ALTER TABLE `issue`
  ADD PRIMARY KEY (`issueID`),
  ADD KEY `assignor` (`assignor`),
  ADD KEY `assignee` (`assignee`),
  ADD KEY `FKj5jkwvvf243vkw3ma2k0f4f9p` (`projectID`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`projectID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `issue`
--
ALTER TABLE `issue`
  ADD CONSTRAINT `FKj5jkwvvf243vkw3ma2k0f4f9p` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`),
  ADD CONSTRAINT `issue_ibfk_1` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`),
  ADD CONSTRAINT `issue_ibfk_2` FOREIGN KEY (`assignor`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `issue_ibfk_3` FOREIGN KEY (`assignee`) REFERENCES `user` (`userID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
