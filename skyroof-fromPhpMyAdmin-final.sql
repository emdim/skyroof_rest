-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 18, 2019 at 11:49 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skyroof`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(44),
(44),
(44);

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

DROP TABLE IF EXISTS `issue`;
CREATE TABLE IF NOT EXISTS `issue` (
  `issueID` bigint(20) NOT NULL,
  `projectID` bigint(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `assignor` bigint(20) NOT NULL,
  `assignee` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `other_details` text,
  PRIMARY KEY (`issueID`),
  KEY `assignor` (`assignor`),
  KEY `assignee` (`assignee`),
  KEY `FKj5jkwvvf243vkw3ma2k0f4f9p` (`projectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `issue`
--

INSERT INTO `issue` (`issueID`, `projectID`, `title`, `description`, `assignor`, `assignee`, `type`, `status`, `other_details`) VALUES
(1, 6, '100kg Bricks', 'We need 100Kg Bricks', 11, 22, 2, 2, 'Up to 30/01/2020'),
(2, 2, 'Renovate Entrance', 'Renovate back entrance', 13, 16, 1, 1, 'Nothing'),
(3, 3, '1 Tone Metal', 'We need more metal for the roof', 14, 13, 0, 0, 'Nothing'),
(4, 4, 'Lightning Rod', 'Add a lightning rod to protect from thunders', 15, 13, 1, 2, 'Nothing'),
(5, 5, '500kg Gravel', 'More gravel needed for the back yard', 16, 21, 2, 1, 'Good Quality'),
(6, 4, 'Not enough bricks', 'More bricks needed', 14, 13, 1, 0, 'Bricks needs to be of high quality.'),
(24, 6, 'Recruiting Employees', 'We need at least 30 employees to finish the project in time', 13, 11, 2, 0, '30 People '),
(41, 5, 'Lift is broken', 'Lift is broken ', 13, 14, 0, 4, ''),
(42, 3, 'Blueprint Confusion', 'Blueprint is not clear', 13, 13, 0, 0, 'Blueprint must be clear'),
(43, 6, 'Wire needed', '15km wire needed', 17, 16, 2, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
CREATE TABLE IF NOT EXISTS `project` (
  `name` varchar(50) DEFAULT NULL,
  `projectID` bigint(20) NOT NULL,
  PRIMARY KEY (`projectID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`name`, `projectID`) VALUES
('Τown Ηall', 1),
('Miami Skyscraper', 2),
('Arab Emirate Palace', 3),
('London Bridge', 4),
('Eiffel Tower', 5),
('Intrasoft Patras', 6);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `userID` bigint(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `permission` int(11) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `username`, `password`, `email`, `permission`) VALUES
(11, 'KostasBen', '$2a$10$CJ.hvgYgEmzNhN6rfAnEReAI8w2FZqtWMIwuEzD8K6qv4.DifNfmy', 'kostas@gmail.com', 1),
(13, 'rtsolka', '$2a$10$0UUVTt4XVHcZQLI8QiOQWeEW2/6Fw099Z45eaQgB8Ar3QKTZdzMP.', 'rtsolka@skyroof.com', 2),
(14, 'ManolisDim', '$2a$10$uSi6jpt6iZH2/yD3En2s2ObSUXhEXNIMwnwcSpC9xUykzsdw03tLy', 'manos_dim@skyroof.com', 2),
(15, 'xaris.Lor', '$2a$10$2K9iC/.HO6fZP7i/Y.SwjejUC1AstEJjL/lpZkmCHRk7tmzKW2zdS', 'xaris@hotmail.com', 1),
(16, 'Bill', '$2a$10$Coql4PDtY6kIknVdOFLe2euqFmY.9gc163a04ctrrWmVEVyb9JPK2', 'skiadas@skyroof.com', 2),
(17, 'ben10', '$2a$10$ouroiu9qLels8XpbXuKoHO5QhXv8W93oHh2wen0D3kt2sk5D7zsuG', 'ben10@gmail.gr', 0),
(18, 'ourania', '$2a$10$P3pmjb2JFOQX9l4EOHqFku7/iK8Fu/EJKnJxfz7tqEAtGGc4UrmuS', 'ourania@intrasoft.gr', 0),
(19, 'emanouil', '$2a$10$BZkAyVGWdQSjJXeY8kAzrOPRLbhWWquNNnfrAhOukyvtryej5TcrK', 'manos@yahoo.gr', 0),
(21, 'xaralampos', '$2a$10$8p6T9GSMWRaNWZOt4mb1ku4RcDC0NArdrwUPNxtviqAzZz1vTkNOW', 'xaris11@hotmail.com', 0),
(22, 'vasilis', '$2a$10$t85Ih9ku/fmQ1O338jIYRu9KWhTJI0oMGgt/JPWf68wO4Sx7ufxqS', 'vasilis@gmail.com', 0),
(33, 'rania.tsolka', '$2a$10$0UUVTt4XVHcZQLI8QiOQWeEW2/6Fw099Z45eaQgB8Ar3QKTZdzMP.', 'rania.tsolka@gmail.com', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `issue`
--
ALTER TABLE `issue`
  ADD CONSTRAINT `FKj5jkwvvf243vkw3ma2k0f4f9p` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`),
  ADD CONSTRAINT `issue_ibfk_1` FOREIGN KEY (`projectID`) REFERENCES `project` (`projectID`),
  ADD CONSTRAINT `issue_ibfk_2` FOREIGN KEY (`assignor`) REFERENCES `user` (`userID`),
  ADD CONSTRAINT `issue_ibfk_3` FOREIGN KEY (`assignee`) REFERENCES `user` (`userID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
